import React from 'react';
import {Dimensions, StyleSheet, Text, View} from "react-native";
import {MaterialIcons} from '@expo/vector-icons';

export default function Header({navigation, title}) {


    const openMenu = () => {

        navigation.openDrawer();
    }

    return (

        <View style={styles.header}>
            <MaterialIcons name='menu' size={28} onPress={openMenu} style={styles.icon}/>

            <View>
                <Text style={styles.headerText}>{title}</Text>
            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    header: {
        width: Dimensions.get('window').width,
        height: '100%',
        flexDirection: 'row',
        justifyContent: 'center'


    },
    headerText: {

        fontWeight: 'bold',
        fontSize: 20,
        color: 'tomato',
        letterSpacing: 1,
    },

    icon: {
        position: 'absolute',
        marginLeft: 10,
        left: 1,
        color: 'tomato'

    }


})


