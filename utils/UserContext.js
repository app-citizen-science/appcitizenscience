import React from "react";

export const UserContext = React.createContext({
    userAuthenticated:null,
    user:null,
    podsNumber:null,
    sodsNumber:null,
    offlineError: false,
    setUserAuthenticated:() => {},
    setUser:() =>{},
    setPodsNumber:() => {},
    setSodsNumber:() => {},
    setOfflineError: ()=>{}
})
