import firebase from 'firebase';
import 'firebase/firestore';

const config = {
    apiKey: "AIzaSyBrXFvNEEBMPbQlRg2W8g9Ta2pB_fl1fxM",
    authDomain: "appcitizenscience.firebaseapp.com",
    projectId: "appcitizenscience",
    storageBucket: "appcitizenscience.appspot.com",
    messagingSenderId: "799801011475",
    appId: "1:799801011475:web:3e4b6f9fda507cbcb5bee6",
    measurementId: "G-12RTTHXXYF"
};

if(!firebase.apps.length){
    firebase.initializeApp(config);
}

export const db = firebase.firestore();

export default firebase;
