import 'firebase/firestore';
import {db} from "./firebase";
import {uploadPodSodImage} from "./storage_requests";
import {locale} from "expo-localization";
import firebase from "firebase";

// FUNCTION TO GET THE POD OF A USER IN FIREBASE
export function myUserPodsStream(observer, userId) {
    return db.collection('pointsOfDifficulty')
        .where('fk_user_id', '==', userId)
        .onSnapshot(observer);
}
// FUNCTION TO GET THE SOD OF A USER IN FIREBASE
export function myUserSodsStream(observer, userId) {
    return db.collection('segmentsOfDifficulty')
        .where('fk_user_id', '==', userId)
        .onSnapshot(observer);
}
// FUNCTION TO GET A SPECIFIC USER
export function getUser(observer, userId){
    return db.collection('users')
        .doc(userId)
        .onSnapshot(observer);
}

// FUNCTION TO GET ALL THE PODs IN FIREBASE
export function allPodsStream(observer) {
    return db.collection('pointsOfDifficulty')
        .onSnapshot(observer);
}

// FUNCTION TO GET THE EMAIL OF A USER BY HIS UID
export function getOwner(userId) {
    return db.collection('users')
        .doc(userId)
        .get()
        .then(doc => doc.data())
        .then(user => user.email);
}

// FUNCTION TO GET A SPECIFIC POD
export function getPod(observer, podId) {
    return db.collection('pointsOfDifficulty')
        .doc(podId)
        .onSnapshot(observer);
}

// FUNCTION TO GET A SPECIFIC SOD
export function getSod(observer, podId) {
    return db.collection('segmentsOfDifficulty')
        .doc(podId)
        .onSnapshot(observer);
}

// FUNCTION TO ADD A POD IN FIREBASE + IMAGE IN FIREBASE STORAGE
export async function addPod(date, description, fk_sod, fk_user, geoPoint, imgDlUrl, riskLvl, technicalLvl, name, podId, status, podImage, userContext) {
    return db.collection("pointsOfDifficulty").add({
        'date': date,
        'description': description,
        'fk_user_id': fk_user,
        'geo_position': geoPoint,
        'img_download_url': imgDlUrl,
        'level_risk': riskLvl,
        'level_technical': technicalLvl,
        'name': name,
        'pod_id': userContext.podsNumber+1,
        'status': status,
        'modification_date': date,
        'language': locale.substring(0, 2),
    }).then((value) => {
        uploadPodSodImage(podImage, value.id, "pods/").then(
            (value2) => {
                updatePodSodImageUrl(value.id, value2, "pointsOfDifficulty");
            }
        );
        console.log('New POD added !');
    });
}

// FUNCTION TO UPDATE POD DETAILS
export function updatePod(doc_id, values, riskLevel, technicalLevel, geoPoint,status) {
    return db.collection("pointsOfDifficulty").doc(doc_id).update({
        'description': values.podDescription,
        'level_risk': riskLevel,
        'level_technical': technicalLevel,
        'name': values.podName,
        'geo_position': geoPoint,
        'status': status,
        'modification_date': new firebase.firestore.FieldValue.serverTimestamp(),
        'language': locale.substring(0, 2),
    }).then(() => {
        console.log('POD updated !');
    });
}

// FUNCTION TO UPDATE SOD DETAILS
export function updateSod(doc_id, values, riskLevel, technicalLevel, geoPointStart, geoPointEnd, status) {
    return db.collection("segmentsOfDifficulty").doc(doc_id).update({
        'description': values.sodDescription,
        'level_risk': riskLevel,
        'level_technical': technicalLevel,
        'name': values.sodName,
        'geo_position_start': geoPointStart,
        'geo_position_end': geoPointEnd,
        'status': status,
        'modification_date': new firebase.firestore.FieldValue.serverTimestamp(),
        'language': locale.substring(0, 2),
    }).then(() => {
        console.log('POD updated !');
    });
}

// FUNCTION TO UPDATE SOD or POD image url
export function updatePodSodImageUrl(doc_id, url, collection) {
    db.collection(collection).doc(doc_id).update({
        'img_download_url': url,
    }).then(() => console.log('POD image url updated.'));
}

// FUNCTION TO DELETE A POD
export function deletePod(doc_id) {
    db.collection("pointsOfDifficulty").doc(doc_id).delete().then(() => {
        console.log("The pod has been deleted successfully.");
    });
}

// FUNCTION TO DELETE A SOD
export function deleteSod(doc_id) {
    db.collection("segmentsOfDifficulty").doc(doc_id).delete().then(() => {
        console.log("The sod has been deleted successfully.");
    });
}

// FUNCTION TO ADD A SOD IN FIREBASE + IMAGE IN FIREBASE STORAGE
export async function addSod(date, description, fk_sod, fk_user, geoPointStart, geoPointEnd, imgDlUrl, riskLvl, technicalLvl, name, podId, status, sodImage, userContext) {
    return db.collection("segmentsOfDifficulty").add({
        'date': date,
        'description': description,
        'fk_user_id': fk_user,
        'geo_position_start': geoPointStart,
        'geo_position_end': geoPointEnd,
        'img_download_url': imgDlUrl,
        'level_risk': riskLvl,
        'level_technical': technicalLvl,
        'name': name,
        'sod_id': userContext.sodsNumber+1,
        'status': status,
        'modification_date': date,
        'language': locale.substring(0, 2),
    }).then((value) => {
        uploadPodSodImage(sodImage, value.id, "sods/").then(
            (value2) => {
                updatePodSodImageUrl(value.id, value2, "segmentsOfDifficulty");
            }
        );
        console.log('New POD added !');
    });
}

// FUNCTION TO GET ALL THE SODs IN FIREBASE
export function allSodsStream(observer) {
    return db.collection('segmentsOfDifficulty')
        .onSnapshot(observer);
}
