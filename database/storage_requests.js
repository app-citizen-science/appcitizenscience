import firebase from "firebase";
import {db} from "./firebase";

// FUNCTION TO UPLOAD THE POD/SOD IMAGE IN FIREBASE STORAGE
export async function uploadPodSodImage(uri, id, path) {
    // Why are we using XMLHttpRequest? See:
    // https://github.com/expo/expo/issues/2402#issuecomment-443726662
    const blob = await new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.onload = function () {
            resolve(xhr.response);
        };
        xhr.onerror = function (e) {
            console.log(e);
            reject(new TypeError('Network request failed'));
        };
        xhr.responseType = 'blob';
        xhr.open('GET', uri, true);
        xhr.send(null);
    });

    const ref = firebase
        .storage()
        .ref()
        .child(path + id + "." + uri.substr(uri.lastIndexOf('.') + 1));
    const snapshot = await ref.put(blob);

    // We're done with the blob, close and release it
    blob.close();

    return await snapshot.ref.getDownloadURL();
}

// FUNCTION TO DELETE THE POD IMAGE IN FIREBASE STORAGE
export async function deletePodImage(pod, doc_id){
    const fileExtension = pod.img_download_url.substring(pod.img_download_url.indexOf('?') - 3, pod.img_download_url.indexOf('?'));

    const ref = await firebase
        .storage()
        .ref()
        .child('pods/' + doc_id + '.' + fileExtension);

    ref.delete();
}

// FUNCTION TO DELETE THE SOD IMAGE IN FIREBASE STORAGE
export async function deleteSodImage(pod, doc_id){
    const fileExtension = pod.img_download_url.substring(pod.img_download_url.indexOf('?') - 3, pod.img_download_url.indexOf('?'));

    const ref = await firebase
        .storage()
        .ref()
        .child('sods/' + doc_id + '.' + fileExtension);

    ref.delete();
}
