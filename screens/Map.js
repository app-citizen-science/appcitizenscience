import * as React from 'react';
import {StyleSheet, View, FlatList, Image, Platform} from 'react-native';
import MapView from 'react-native-maps';
import {UrlTile, PROVIDER_GOOGLE, Marker, Polyline} from 'react-native-maps';
import {useContext, useEffect, useState} from "react";
import {allPodsStream, allSodsStream, getOwner} from "../database/firestore_requests";
import {mapDocToPod, mapDocToSod} from "../database/maping_methods";
import {t} from "../languages";
import {ListItem, SearchBar} from "react-native-elements";
import {locale} from "expo-localization";
import ConnectionManagement from "../components/ConnectionManagement";
import {UserContext} from "../utils/UserContext";
import {useIsConnected} from "react-native-offline";
import {SodDetailsMap} from "../components/SodDetailsMap";
import {PodDetailsMap} from "../components/PodDetailsMap";
import {MaterialCommunityIcons} from '@expo/vector-icons';


export function MapScreen({navigation}) {
    //States
    const [allPods, setAllPods] = useState([]);
    const [allSods, setAllSods] = useState([]);
    const [searchText, setSearchText] = useState('');
    const [searchResults, setSearchResults] = useState([]);
    const [position, setPosition] = useState({
        latitude: 46.484,
        longitude: 8.1336,
        latitudeDelta: 3,
        longitudeDelta: 3
    });
    const [podSelected, setPodSelected] = useState();
    const [sodSelected, setSodSelected] = useState();
    const [ownerElementSelected, setOwnerElementSelected] = useState();

    //Hooks
    const userContext = useContext(UserContext);
    const isConnected = useIsConnected();

    useEffect(() => {
        //Get a list of the pods created by the user connected from firestore and map every document to pod objects
        //Subscribe to any changes of the data and unsubscribe when not necessary anymore
        const unsubscribe = allPodsStream({
            next: querySnapshot => {
                const userPodsFirestore = querySnapshot.docs
                    .map((docSnapshot, index) => mapDocToPod(docSnapshot));
                userPodsFirestore.sort((a, b) => {
                    return a.date - b.date
                });
                if (userPodsFirestore.length === 0) {
                    if (!isConnected) {
                        userContext.setOfflineError(true);
                    }
                }
                setAllPods(userPodsFirestore);

                //Refreshing the pod details if it is opened and the pod has been modified
                if (podSelected !== undefined) {
                    let podFromFirestore = userPodsFirestore.find((pod) => pod.doc_id === podSelected.doc_id);
                    if (podFromFirestore !== undefined) {
                        if (podFromFirestore.modification_date !== podSelected.modification_date) {
                            setPodSelected(podFromFirestore);
                            setPosition({
                                latitude: podFromFirestore.geo_position.latitude - 0.0002,
                                longitude: podFromFirestore.geo_position.longitude,
                                latitudeDelta: 0.001,
                                longitudeDelta: 0.001
                            });
                        }
                    }
                }
            },
            error: (error) => {
                if (!isConnected) {
                    userContext.setOfflineError(true);
                }
                setAllPods([]);
            }
        });
        return () => unsubscribe();
    }, [setAllPods]);

    useEffect(() => {
        //Get a list of the SODS created by the user connected from firestore and map every document to sod objects
        //Subscribe to any changes of the data and unsubscribe when not necessary anymore
        const unsubscribe = allSodsStream({
            next: querySnapshot => {
                const userSodsFirestore = querySnapshot.docs
                    .map((docSnapshot, index) => mapDocToSod(docSnapshot));
                userSodsFirestore.sort((a, b) => {
                    return a.date - b.date
                });
                if (userSodsFirestore.length === 0) {
                    if (!isConnected) {
                        userContext.setOfflineError(true);
                    }
                }
                setAllSods(userSodsFirestore);

                //Refreshing the sod details if it is opened and the sod has been modified
                if (sodSelected !== undefined) {
                    let sodFromFirestore = userSodsFirestore.find((sod) => sod.doc_id === sodSelected.doc_id);
                    if (sodFromFirestore !== undefined) {
                        if (sodFromFirestore.modification_date !== sodSelected.modification_date) {
                            setSodSelected(sodFromFirestore);
                            calculateCenterPosition(sodFromFirestore);
                        }
                    }
                }
            },
            error: (error) => {
                if (!isConnected) {
                    userContext.setOfflineError(true);
                }
                setAllSods([]);
            }
        });
        return () => unsubscribe();
    }, [setAllSods]);

    let sodIcon = {
        green: require('../assets/sod_green.png'),
        blue: require('../assets/sod_blue.png'),
        red: require('../assets/sod_red.png'),
        orange: require('../assets/sod_orange.png'),
        black: require('../assets/sod_black.png')
    }

    function definePinColor(status) {
        //Change the color of the marker on the map according to the status of the pod or the sod
        let statusToWatch = status;

        if (statusToWatch === 'validated') {
            return 'green';
        }
        if (statusToWatch === 'toValidate') {
            return 'blue';
        }
        if (statusToWatch === 'toChange') {
            return 'orange';
        }
        if (statusToWatch === 'rejected') {
            return 'red';
        }
        return 'black';
    }

    function searchLocations(searchText) {
        if (podSelected !== undefined || sodSelected !== undefined) {
            setPodSelected(undefined);
            setSodSelected(undefined);
        }
        //Use the swisstopo api to make the research of a location
        setSearchText(searchText);

        //Get locale language from device to make the request
        let lng = locale.substring(0, 2);
        if (lng !== 'fr' || lng !== 'en' || lng !== 'de' || lng !== 'it') {
            lng = 'en';
        }

        if (searchText.length > 2) {
            const url = 'https://api3.geo.admin.ch/rest/services/api/SearchServer?searchText=\'' + searchText + '\'&type=locations&limit=10&sr=3857&lang=' + lng
            fetch(url)
                .then((response) => response.json())
                .then((responseJson) => {
                    setSearchResults(responseJson.results);
                });
        } else {
            setSearchResults([]);
        }

    }

    function handleChooseLocation(locationChosen) {
        //Reset the results and the search text to remove the list component frontward the map
        setSearchResults([]);
        setSearchText('');

        //Modify the zoom level of the map according to the result type
        let zoom = 0.06;

        if (locationChosen.attrs.origin === 'address' || locationChosen.attrs.origin === 'gazetteer') {
            zoom = 0.0035;
        }
        //Setting the new position to show in the map
        const newPosition = {
            latitude: locationChosen.attrs.lat,
            longitude: locationChosen.attrs.lon,
            latitudeDelta: zoom,
            longitudeDelta: zoom
        };
        setPosition(newPosition);
    }

    function displayPodDetails(event, pod) {
        //Display the pod details when his marker has been clicked on the map and unselect sod
        event.preventDefault();
        getOwner(pod.fk_user_id).then(email => setOwnerElementSelected(email));
        setPosition({
            latitude: pod.geo_position.latitude - 0.0002,
            longitude: pod.geo_position.longitude,
            latitudeDelta: 0.001,
            longitudeDelta: 0.001
        });
        setPodSelected(pod);
        setSodSelected(undefined);
    }

    function calculateCenterPosition(sod) {
        //This function calculate the latitudeDelta and longitudeDelta according to the distance of the sod extremities
        //The idea of this function is to try to display the two sod extremities on the screen
        const longitudeStart = sod.geo_position_start.longitude;
        const latitudeStart = sod.geo_position_start.latitude;
        const longitudeEnd = sod.geo_position_end.longitude;
        const latitudeEnd = sod.geo_position_end.latitude;

        //Take the lowest latitude point between the two positions
        let lowestLatitude;
        if (latitudeStart >= latitudeEnd) {
            lowestLatitude = latitudeEnd;
        } else {
            lowestLatitude = latitudeStart;
        }

        //Take the lowest longitude point between the two positions
        let lowestLongitude;
        if (longitudeStart >= longitudeEnd) {
            lowestLongitude = longitudeEnd;
        } else {
            lowestLongitude = longitudeStart;
        }

        //Calculate difference between the two longitude
        let distanceLongitudes = longitudeEnd - longitudeStart;
        if (distanceLongitudes < 0) {
            distanceLongitudes = distanceLongitudes * (-1);
        }

        //Calculate difference between the two latitudes
        let distanceLatitudes = latitudeEnd - latitudeStart;
        if (distanceLatitudes < 0) {
            distanceLatitudes = distanceLatitudes * (-1);
        }

        setPosition({
            latitude: lowestLatitude - 0.0002,
            longitude: lowestLongitude,
            latitudeDelta: distanceLatitudes * 3,
            longitudeDelta: distanceLongitudes * 3
        });
    }

    function displaySodDetails(event, sod) {
        //Display the sod details when his marker has been clicked on the map and unselect pod
        event.preventDefault();
        getOwner(sod.fk_user_id).then(email => setOwnerElementSelected(email));
        calculateCenterPosition(sod);
        setPodSelected(undefined);
        setSodSelected(sod);
    }

    function closeDetailsPanel(event) {
        //Close the panel of the Details
        event.preventDefault();
        setPodSelected(undefined);
        setSodSelected(undefined);
    }

    return (
        <View style={{flex: 1, backgroundColor: 'tomato', height: '90%'}}>
            <View style={styles.mapContainer}>
                <ConnectionManagement/>
                {isConnected ? (
                    <View style={{position: 'absolute', top: 0, zIndex: 150}}>
                        <SearchBar
                            containerStyle={{backgroundColor: 'transparent',}}
                            placeholder={t('_search')}
                            lightTheme={true}
                            platform={"ios"}
                            onChangeText={(value) => {
                                searchLocations(value);
                            }}
                            value={searchText}
                        />

                        <FlatList data={searchResults}
                                  keyExtractor={item => item.id.toString()}
                                  renderItem={(item, index) => {
                                      const regex = /(<([^>]+)>)/ig;
                                      const result = item.item.attrs.label.replace(regex, '');
                                      return (
                                          <ListItem
                                              key={'searchresult-' + index}
                                              onPress={() => handleChooseLocation(item.item)}
                                              bottomDivider
                                          >
                                              <ListItem.Title>{result}</ListItem.Title>
                                          </ListItem>
                                      );
                                  }
                                  }/>

                    </View>
                ) : null}

                <View style={{flex: 1}}>
                    <MapView
                        style={styles.map}
                        mapType={isConnected ? "none" : "standard"}
                        maxZoomLevel={17}
                        minZoomLevel={7}
                        region={position}
                        onRegionChangeComplete={(region) => {
                            setPosition(region);
                        }}
                        loadingEnabled={true}
                        showsUserLocation={true}
                        provider={PROVIDER_GOOGLE}
                        showsMyLocationButton={true}
                    >
                        <UrlTile
                            urlTemplate={'https://wmts.geo.admin.ch/1.0.0/ch.swisstopo.pixelkarte-grau/default/current/3857/{z}/{x}/{y}.jpeg'}
                            zIndex={1}
                        />
                        <UrlTile
                            urlTemplate='https://wmts.geo.admin.ch/1.0.0/ch.swisstopo.swisstlm3d-wanderwege/default/current/3857/{z}/{x}/{y}.png'
                            zIndex={2}
                        />

                        {allPods.length !== 0 ? allPods.map((pod, index) => (
                            <Marker key={index}
                                    coordinate={{
                                        latitude: pod.geo_position.latitude,
                                        longitude: pod.geo_position.longitude
                                    }}
                                    zIndex={10}
                                    pinColor={definePinColor(pod.status)}
                                    onPress={event => displayPodDetails(event, pod)}
                            />
                        )) : null}
                        {allSods.length !== 0 ? allSods.map((sod, index) => (
                            (sodSelected !== undefined && sod.doc_id === sodSelected.doc_id ? null :
                                <Marker key={index}
                                        coordinate={{
                                            latitude: (sod.geo_position_start.latitude + sod.geo_position_end.latitude) / 2,
                                            longitude: (sod.geo_position_start.longitude + sod.geo_position_end.longitude) / 2
                                        }}
                                        zIndex={10}
                                        onPress={event => displaySodDetails(event, sod)}
                                >
                                    <MaterialCommunityIcons
                                        name="map-marker-distance"
                                        size={44}
                                        color={definePinColor(sod.status)}/>
                                </Marker>)
                        )) : null}
                        {sodSelected === undefined ? null : (
                            <>
                                <Marker key={'sod-marker-start-' + sodSelected.doc_id}
                                        coordinate={{
                                            latitude: sodSelected.geo_position_start.latitude,
                                            longitude: sodSelected.geo_position_start.longitude
                                        }}
                                        zIndex={10}
                                        pinColor={definePinColor(sodSelected.status)}
                                        onPress={event => closeDetailsPanel(event)}
                                />
                                <Marker key={'sod-marker-end-' + sodSelected.doc_id}
                                        coordinate={{
                                            latitude: sodSelected.geo_position_end.latitude,
                                            longitude: sodSelected.geo_position_end.longitude
                                        }}
                                        zIndex={10}
                                        pinColor={definePinColor(sodSelected.status)}
                                        onPress={event => closeDetailsPanel(event)}
                                />
                                <Polyline
                                    zIndex={10}
                                    strokeColor="tomato"
                                    strokeWidth={5}
                                    coordinates={[
                                        {
                                            latitude: sodSelected.geo_position_end.latitude,
                                            longitude: sodSelected.geo_position_end.longitude
                                        },
                                        {
                                            latitude: sodSelected.geo_position_start.latitude,
                                            longitude: sodSelected.geo_position_start.longitude
                                        }
                                    ]}
                                />
                            </>)
                        }


                    </MapView>
                </View>
                {sodSelected === undefined ? null :
                    <View style={styles.detailsPanel}>
                        <SodDetailsMap sod={sodSelected} closeDetailsPanel={closeDetailsPanel}
                                       ownerElement={ownerElementSelected} navigation={navigation}/>
                    </View>}
                {podSelected === undefined ? null :
                    <View style={styles.detailsPanel}>
                        <PodDetailsMap pod={podSelected} closeDetailsPanel={closeDetailsPanel}
                                       ownerElement={ownerElementSelected} navigation={navigation}/>
                    </View>}
            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    mapContainer: {
        width: "100%",
        height: "100%",
        position: 'relative'
    },
    map: {
        width: "100%",
        height: "100%",
        backgroundColor: '#ffffff'
    },
    detailsPanel: {
        flex: 1,
        position: "absolute",
        width: '100%',
        height: '50%',
        backgroundColor: '#eaeaea',
        bottom: 0,
        borderTopRightRadius: 40,
        borderTopLeftRadius: 40,
        paddingHorizontal: 10,
        zIndex: 200
    },
    markerIcon: {
        width: 36,
        height: 36
    }
});


