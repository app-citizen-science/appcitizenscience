import * as React from 'react';
import {Text, View, Image, ScrollView} from "react-native";
import ActionButton from "react-native-action-button";
import {useState, useEffect, useContext} from "react";
import {ListItem} from "react-native-elements";
import {Ionicons, MaterialCommunityIcons, FontAwesome5} from '@expo/vector-icons';
import {MyPodDetails} from "./MyPodDetails";
import {myUserPodsStream} from "../database/firestore_requests";
import {mapDocToPod} from "../database/maping_methods";
import firebase from "firebase";
import ConnectionManagement from "../components/ConnectionManagement";
import {useIsConnected} from "react-native-offline";
import {UserContext} from "../utils/UserContext";
import {t} from "../languages";

export function MyPods({navigation}) {
    let [userPods, setUserPods] = useState([]);

    //Get the uid of the user connected
    let userId = firebase.auth().currentUser.uid;

    let userContext = useContext(UserContext);

    useEffect(() => {
        //Get a list of the pods created by the user connected from firestore and map every document to pod objects
        //Subscribe to any changes of the data and unsubscribe when not necessary anymore
        const unsubscribe = myUserPodsStream({
            next: querySnapshot => {
                const userPodsFirestore = querySnapshot.docs
                    .map((docSnapshot, index) => {
                        if (docSnapshot.data().pod_id !== undefined) {
                            return mapDocToPod(docSnapshot);
                        }
                    });
                userPodsFirestore.sort((a, b) => {
                    return a.date - b.date
                });

                if(isConnected){
                    if (userPodsFirestore.length === 0 || userPodsFirestore.length === null){
                        userContext.setPodsNumber(0);
                        setUserPods([]);
                    }
                    else{
                        setUserPods(userPodsFirestore);
                        userContext.setPodsNumber(userPodsFirestore[userPodsFirestore.length-1].pod_id);
                    }
                }

            },
            error: (error) => {
                setUserPods([]);
            }
        }, userId);
        return () => unsubscribe();
    }, [setUserPods]);

    const isConnected = useIsConnected();
    return (
        <View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'stretch'}}>
            <ConnectionManagement/>
            <ScrollView>

                {userPods.length !== 0 ? userPods.map((value, index) => (
                    <ListItem key={index}
                              style={{margin: 2}}
                              onPress={() => {
                                  if (isConnected) {
                                      navigation.navigate('MyPodDetails', {doc_id: value.doc_id})
                                  }
                              }}
                    >
                        {value.img_download_url !== '' ?
                            <Image style={{width: 75, height: 75, margin: 5}}
                                   source={{uri: value.img_download_url, cache: 'force-cache'}}/>
                            :
                            <View style={{width: 75, height: 75, alignContent: "center"}}>
                                <Ionicons name="image-outline" size={75} color="grey"/>
                            </View>}
                        <ListItem.Content>
                            <ListItem.Title>
                                <View style={{flex: 1, flexDirection: 'row'}}>
                                    <Text style={{fontWeight: 'bold', marginBottom: 10}}>
                                        {value.pod_id} - {value.name}
                                    </Text>
                                    {value.status === 'validated' ?
                                        <Ionicons name="checkmark-circle" size={18} color="green"
                                                  style={{marginLeft: 5}}/>
                                        :
                                        value.status === 'toValidate' ?
                                            <MaterialCommunityIcons name="clock-time-four" size={18} color="blue"
                                                                    style={{marginLeft: 5}}/>
                                            :
                                            value.status === 'rejected' ?
                                                <Ionicons name="close-circle" size={18} color="red"
                                                          style={{marginLeft: 5}}/> :
                                                value.status === 'toChange' ?
                                                    <MaterialCommunityIcons name="pencil-circle" size={18}
                                                                            color="orange"
                                                                            style={{marginLeft: 5}}/>
                                                    : null
                                    }
                                </View>
                            </ListItem.Title>
                            <ListItem.Subtitle>{value.date === null ? (
                                <Text>Loading...</Text>) : value.date.toDate().toLocaleDateString('fr-ch')}</ListItem.Subtitle>
                        </ListItem.Content>
                        <ListItem.Chevron/>
                    </ListItem>
                )) : (
                    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', minHeight: '10%'}}>
                        <FontAwesome5 name="hiking" size={90} color="tomato"/>
                        <Text style={{color: 'tomato', marginTop: 10, fontWeight: 'bold'}}>{t("_firstPod")}</Text>
                    </View>
                )}

            </ScrollView>
            <ActionButton buttonColor="rgba(231,76,60,1)" onPress={() => navigation.navigate('New POD')}/>
        </View>

    );
}
