import React from 'react';
import {View} from 'react-native';
import PDFReader from "rn-pdf-reader-js";


export default function Risk() {

    return (
        <View style={{flex: 1}}>
            <PDFReader source={{uri: 'https://firebasestorage.googleapis.com/v0/b/appcitizenscience.appspot.com/o/documentation%2FRisque.pdf?alt=media&token=62e36831-69bf-4b13-b7fa-052862923063', cache: true}}/>
        </View>
    )
}

