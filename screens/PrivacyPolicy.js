import Markdown from 'react-native-markdown-display';
import React from "react";
import {ScrollView} from "react-native";
import {locale} from "expo-localization";
import {fr, de, it, en} from "../languages/legal/privacyPolicy.json";

export function PrivacyPolicy() {
    //Select privacy policy according to the language of the phone
    let lang = locale.substring(0, 2);
    if(lang !=='fr' && lang !=='de' && lang !=='it' && lang !=='en'){
        lang='en';
    }
    const privacyPolicy = {
        fr: fr,
        en: en,
        de: de,
        it: it
    }

    return (
        <>
            <ScrollView style={{padding:10, marginBottom:50}}>
                <Markdown>
                    {privacyPolicy[lang]}
                </Markdown>
            </ScrollView>
        </>
    );
}
/*

*/
