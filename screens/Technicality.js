import React from 'react';
import {StyleSheet, View, Text, Image, ScrollView} from 'react-native';
import {t} from "../languages";
import PDFReader from "rn-pdf-reader-js";


export default function Technicality() {

    return (
        <View style={{flex: 1}}>
                <PDFReader source={{uri: 'https://firebasestorage.googleapis.com/v0/b/appcitizenscience.appspot.com/o/documentation%2FTechnicite.pdf?alt=media&token=91ea7ee3-523e-41b1-b660-9792f36c179c', cache: true}}/>
        </View>
    )
}

