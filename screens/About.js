import React from 'react';
import {StyleSheet, View, Text, Image, ScrollView} from 'react-native';
import {t} from "../languages";
import Constants from "expo-constants";


export default function About({navigation}) {

    return (
        <ScrollView style={styles.container}>
            <View style={styles.iconsContainer}>
                <Image source={require('../assets/icon_santour.png')} style={styles.appIcon}/>
                <Image source={require('../assets/icon_hessovs.png')} style={styles.hevsIcon}/>
            </View>
            <View>
                <View style={styles.textContainer}>
                    <Text style={styles.textStyle}>{t('_aboutText')}</Text>
                </View>
                <View style={styles.textContainer}>
                    <Text
                        style={styles.textLinkStyle}
                        onPress={() => navigation.navigate('Legal')}>
                        {t("_privacyPolicyAndTermsConditions")}
                    </Text>
                </View>
                <View style={styles.versionContainer}>
                    <Text
                        style={styles.textStyle}>
                        {t('_version')} {Constants.manifest.version}
                    </Text>
                </View>

            </View>
        </ScrollView>

    )
}

const styles = StyleSheet.create({
    iconsContainer: {
        flexDirection: 'row',
        width: '100%',
        marginTop: 50,
        marginBottom: 30,
        justifyContent:'center'
    },
    appIcon: {
        width: 120,
        height: 120,
        borderRadius: 10,
    },
    hevsIcon: {
        width: 120,
        height: 120,
        marginLeft: '10%',
        borderRadius: 10,
    },
    textContainer: {
        marginBottom: 5,
        marginHorizontal: 30,
        backgroundColor: 'white',
        borderRadius: 10
    },
    versionContainer: {
        marginBottom: 20,
        alignSelf:'center',
    },
    textStyle: {
        padding: 20,
        color: 'grey'
    },
    textLinkStyle: {
        padding: 20,
        color: 'tomato'
    },
    container: {
        flex: 1
    }
});

