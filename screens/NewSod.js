import * as React from 'react';
import {
    Alert,
    Keyboard,
    View,
    Text,
    StyleSheet,
    Image,
    ScrollView,
    ActivityIndicator,
} from "react-native";
import {Formik} from 'formik';
import {Button, TextInput} from 'react-native-paper';
import {useContext, useEffect, useState} from "react";
import * as ImagePicker from "expo-image-picker";
import DatePicker from 'react-native-datepicker';
import firebase from "firebase";
import * as yup from 'yup';
import {t} from "../languages";
import Icon from 'react-native-vector-icons/Ionicons';
import {addSod} from "../database/firestore_requests";
import MapView, {Marker, PROVIDER_GOOGLE, UrlTile} from "react-native-maps";
import {useIsConnected} from "react-native-offline";
import {UserContext} from "../utils/UserContext";
import {TouchableOpacity} from 'react-native-gesture-handler';

/*
    Class to create a new SOD
*/

export const latitudeDeltaZoomMin = 0.005;
export const longitudeDeltaZoomMin = 0.0061;

export function NewSodScreen({navigation}) {

    const [image, setImage] = useState(null);
    const [sodDate, setSodDate] = useState(new Date());
    const [riskLevel, setRiskLevel] = useState(0);
    const [technicalLevel, setTechnicalLevel] = useState(0);
    const [status, setStatus] = useState(null);

    let userContext = useContext(UserContext);
    let isConnected = useIsConnected();

    let [position, setPosition] = useState({
        latitude: 46.484,
        longitude: 8.1336,
        latitudeDelta: 3,
        longitudeDelta: 3
    });

    const levelColor = {
        one: 'green',
        two: 'blue',
        three: 'orange',
        four: 'red',
        five: 'black'
    }

    useEffect(() => {
        //Get the coordinates
        findCoordinates('start');

        //Check the user role
        if (userContext.user.role === 'Beginner') {
            setStatus('toValidate');
        } else {
            setStatus('validated');
        }

        if (userContext.user.role === 'Advanced' && !isConnected) {
            setStatus('toValidate');
        }
    }, []);

    //Image picker from Library
    const pickImage = async () => {
        await (async () => {
            if (Platform.OS !== 'web') {
                const responseLibraryPermission = await ImagePicker.requestMediaLibraryPermissionsAsync();
                if (responseLibraryPermission.status !== 'granted') {
                    alert(t("_cameraPermission"));
                }
            }
        })();

        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            aspect: [4, 3],
            quality: 1,
        });

        if (!result.cancelled) {
            setImage(result.uri);
        }
    };

    //Camera picker
    const pickCamera = async () => {
        await (async () => {
            if (Platform.OS !== 'web') {
                const responseCameraPermission = await ImagePicker.requestCameraPermissionsAsync();
                if (responseCameraPermission.status !== 'granted') {
                    alert(t("_cameraPermission"));
                }
            }
        })();

        let result = await ImagePicker.launchCameraAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            aspect: [4, 3],
            quality: 1,
        });

        if (!result.cancelled) {
            setImage(result.uri);
        }
    };

    const [startLatitude, setStartLatitude] = useState(0);
    const [startLongitude, setStartLongitude] = useState(0);
    const [endLatitude, setEndLatitude] = useState(0);
    const [endLongitude, setEndLongitude] = useState(0);
    const [coordinateGetted, setCoordinateGetted] = useState(false);

    //Method to find the user coordinates
    const findCoordinates = (point) => {
        navigator.geolocation.getCurrentPosition(
            position => {
                if (point === 'start') {
                    setStartLatitude(position.coords.latitude);
                    setStartLongitude(position.coords.longitude);
                }

                if (point === 'end') {
                    setEndLatitude(position.coords.latitude);
                    setEndLongitude(position.coords.longitude);
                }

                setPosition({
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: 0.002,
                        longitudeDelta: 0.002
                    }
                );
                setCoordinateGetted(true);
            },
            error => Alert.alert(error.message),
            {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
        );
    };

    //Yup validation
    const formValidationSchema = yup.object().shape({
        sodName: yup
            .string()
            .required(t("_sodNameRequired")),
    })

    return (
        <View style={styles.container}>
            <View style={styles.content}>
                <Formik
                    validationSchema={formValidationSchema}
                    initialValues={{
                        sodName: '',
                        sodDescription: '',
                        sodStartLatitude: startLatitude,
                        sodEndLatitude: endLatitude,
                        sodStartLongitude: startLongitude,
                        sodEndLongitude: endLongitude,
                        techniqueLvl: '',
                        riskLvl: '',
                        sodDate: ''
                    }}
                    onSubmit={async values => {

                        if (userContext.user.role === 'Advanced' && !isConnected) {
                            console.log("ADVANCED");
                            Alert.alert(
                                t("_newSod"),
                                t("_advancedUserNewSod"),
                                [
                                    {
                                        text: t("_ok"),
                                        onPress: () => {
                                            console.log('Pick canceled')
                                        },
                                        style: "cancel"
                                    }
                                ]
                            )
                        }

                        navigation.navigate('My SOD');

                        // DATABASE REQUEST - POST/ADD SOD
                        await addSod(
                            new firebase.firestore.FieldValue.serverTimestamp(),
                            values.sodDescription,
                            '',
                            firebase.auth().currentUser.uid,
                            new firebase.firestore.GeoPoint(startLatitude, startLongitude),
                            new firebase.firestore.GeoPoint(endLatitude, endLongitude),
                            '',
                            riskLevel,
                            technicalLevel,
                            values.sodName,
                            '',
                            status,
                            image,
                            userContext
                        );

                        userContext.setSodsNumber(userContext.sodsNumber + 1);

                        Keyboard.dismiss();
                    }
                    }>
                    {({handleChange, errors, handleSubmit, values}) => (
                        <ScrollView
                            // style={{height:'102%'}}
                        >
                            <TouchableOpacity style={styles.touchableZoneImage}
                                              onPress={() => {
                                                  Alert.alert(
                                                      t("_addImageAlertTitle"),
                                                      t("_addImageAlertMessage"),
                                                      [
                                                          {
                                                              text: t("_cameraRoll"),
                                                              onPress: () => (pickImage())
                                                          },
                                                          {
                                                              text: t("_takePhoto"),
                                                              onPress: () => (pickCamera())
                                                          },
                                                          {
                                                              text: t("_cancel"),
                                                              onPress: () => {
                                                                  console.log('Pick canceled')
                                                              },
                                                              style: "cancel"
                                                          }
                                                      ]
                                                  )
                                              }}
                            >
                                <View>
                                    {
                                        image
                                            ?
                                            <View style={styles.imageContainer}>
                                                <Image source={{uri: image, cache: 'force-cache'}}
                                                       style={styles.imageSize}/>
                                            </View>
                                            :

                                            <View style={{alignItems: 'center', justifyContent: 'center'}}>
                                                <Icon name="camera" style={{color: 'tomato'}} size={100}/>
                                                <Text>{t("_tapToAddPicture")}</Text>
                                            </View>
                                    }
                                </View>
                            </TouchableOpacity>

                            {/*SOD NAME*/}
                            <TextInput
                                name="sodName"
                                style={styles.input}
                                theme={inputTheme}
                                onChangeText={handleChange('sodName')}
                                value={values.sodName}
                                label={t("_sodName")}
                                underlineColor="tomato"
                                selectionColor="tomato"
                                underlineColorAndroid="tomato"
                                testID='sodNameInput'
                                keyboardType="default"
                                returnKeyType="done"
                                blurOnSubmit={true}
                                onSubmitEditing={() => {
                                    Keyboard.dismiss()
                                }}
                            />
                            {errors.sodName &&
                            <Text style={{fontSize: 10, color: 'red'}}>{errors.sodName}</Text>
                            }

                            {/*SOD DESCRIPTION*/}
                            <TextInput
                                style={styles.input}
                                theme={inputTheme}
                                onChangeText={handleChange('sodDescription')}
                                value={values.sodDescription}
                                label={t("_sodDescription")}
                                underlineColor="tomato"
                                selectionColor="tomato"
                                multiline={true}
                                keyboardType="default"
                                returnKeyType="done"
                                blurOnSubmit={true}
                                onSubmitEditing={() => {
                                    Keyboard.dismiss()
                                }}
                            />

                            {/*RISK LEVEL MANAGEMENT*/}
                            <Text style={styles.formTitle}>
                                {
                                    renderLevels(riskLevel, t("_riskLevel"))
                                }
                            </Text>
                            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                                <View style={{height: 50}}>
                                    {
                                        riskLevel != 1
                                            ? <TouchableOpacity onPress={() => {
                                                setRiskLevel(1);
                                            }}>
                                                <Button mode="contained" color="grey">
                                                    1
                                                </Button>
                                            </TouchableOpacity>
                                            : <Button mode="contained" color={levelColor.one}>
                                                1
                                            </Button>
                                    }
                                </View>
                                <View style={{height: 50}}>
                                    {
                                        riskLevel != 2
                                            ? <TouchableOpacity onPress={() => {
                                                setRiskLevel(2);
                                            }}>
                                                <Button mode="contained" color="grey">
                                                    2
                                                </Button>
                                            </TouchableOpacity>
                                            : <Button mode="contained" color={levelColor.two}>
                                                2
                                            </Button>
                                    }
                                </View>
                                <View style={{height: 50}}>
                                    {
                                        riskLevel != 3
                                            ? <TouchableOpacity onPress={() => {
                                                setRiskLevel(3);
                                            }}>
                                                <Button mode="contained" color="grey">
                                                    3
                                                </Button>
                                            </TouchableOpacity>
                                            : <Button mode="contained" color={levelColor.three}>
                                                3
                                            </Button>
                                    }
                                </View>
                                <View style={{height: 50}}>
                                    {
                                        riskLevel != 4
                                            ? <TouchableOpacity onPress={() => {
                                                setRiskLevel(4);
                                            }}>
                                                <Button mode="contained" color="grey">
                                                    4
                                                </Button>
                                            </TouchableOpacity>
                                            : <Button mode="contained" color={levelColor.four}>
                                                4
                                            </Button>
                                    }
                                </View>
                                <View style={{height: 50}}>
                                    {
                                        riskLevel != 5
                                            ? <TouchableOpacity onPress={() => {
                                                setRiskLevel(5);
                                            }}>
                                                <Button mode="contained" color="grey">
                                                    5
                                                </Button>
                                            </TouchableOpacity>
                                            : <Button mode="contained" color={levelColor.five}>
                                                5
                                            </Button>
                                    }
                                </View>
                            </View>

                            {/*TECHNICAL LEVEL MANAGEMENT*/}
                            <Text style={styles.formTitle}>
                                {
                                    renderLevels(technicalLevel, t("_technicalLevel"))
                                }
                            </Text>
                            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                                <View style={{height: 50}}>
                                    {
                                        technicalLevel != 1
                                            ? <TouchableOpacity onPress={() => {
                                                setTechnicalLevel(1);
                                            }}>
                                                <Button mode="contained" color="grey">
                                                    1
                                                </Button>
                                            </TouchableOpacity>
                                            : <Button mode="contained" color={levelColor.one}>
                                                1
                                            </Button>
                                    }
                                </View>
                                <View style={{height: 50}}>
                                    {
                                        technicalLevel != 2
                                            ? <TouchableOpacity onPress={() => {
                                                setTechnicalLevel(2);
                                            }}>
                                                <Button mode="contained" color="grey">
                                                    2
                                                </Button>
                                            </TouchableOpacity>
                                            : <Button mode="contained" color={levelColor.two}>
                                                2
                                            </Button>
                                    }
                                </View>
                                <View style={{height: 50}}>
                                    {
                                        technicalLevel != 3
                                            ? <TouchableOpacity onPress={() => {
                                                setTechnicalLevel(3);
                                            }}>
                                                <Button mode="contained" color="grey">
                                                    3
                                                </Button>
                                            </TouchableOpacity>
                                            : <Button mode="contained" color={levelColor.three}>
                                                3
                                            </Button>
                                    }
                                </View>
                                <View style={{height: 50}}>
                                    {
                                        technicalLevel != 4
                                            ? <TouchableOpacity onPress={() => {
                                                setTechnicalLevel(4);
                                            }}>
                                                <Button mode="contained" color="grey">
                                                    4
                                                </Button>
                                            </TouchableOpacity>
                                            : <Button mode="contained" color={levelColor.four}>
                                                4
                                            </Button>
                                    }
                                </View>
                                <View style={{height: 50}}>
                                    {
                                        technicalLevel != 5
                                            ? <TouchableOpacity onPress={() => {
                                                setTechnicalLevel(5);
                                            }}>
                                                <Button mode="contained" color="grey">
                                                    5
                                                </Button>
                                            </TouchableOpacity>
                                            : <Button mode="contained" color={levelColor.five}>
                                                5
                                            </Button>
                                    }
                                </View>
                            </View>

                            {/*COORDINATES MANAGEMENT*/}
                            <Text style={styles.formTitle}>Markers</Text>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'stretch',}}>
                                <View style={styles.coordinates}>
                                    {
                                        coordinateGetted
                                            ? <TouchableOpacity
                                                onPress={() => {
                                                    setCoordinateGetted(false);
                                                    findCoordinates('start');
                                                }}
                                            >
                                                <Button mode="contained" color="green"
                                                        style={styles.button}>{t("_startPoint")}</Button>
                                            </TouchableOpacity>
                                            : <TouchableOpacity
                                                onPress={() => {
                                                    setCoordinateGetted(false);
                                                    findCoordinates('start');
                                                }}
                                            >
                                                <Button mode="contained" color="green"
                                                        disabled
                                                        style={styles.button}>{t("_startPoint")}</Button>
                                            </TouchableOpacity>
                                    }
                                </View>
                                <View style={{
                                    width: '20%', height: 45, justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                    {
                                        coordinateGetted
                                            ? null
                                            : <ActivityIndicator size="large" color="tomato"></ActivityIndicator>
                                    }
                                </View>
                                <View style={styles.coordinates}>
                                    {
                                        coordinateGetted
                                            ? <TouchableOpacity
                                                onPress={() => {
                                                    setCoordinateGetted(false);
                                                    findCoordinates('end');
                                                }}
                                            >
                                                <Button mode="contained" color="red"

                                                        style={styles.button}>{t("_endPoint")}</Button>
                                            </TouchableOpacity>
                                            : <TouchableOpacity
                                                onPress={() => {
                                                    setCoordinateGetted(false);
                                                    findCoordinates('end');
                                                }}
                                            >
                                                <Button mode="contained" color="red"
                                                        disabled

                                                        style={styles.button}>{t("_endPoint")}</Button>
                                            </TouchableOpacity>
                                    }
                                </View>
                            </View>

                            {/*SEPARATOR*/}
                            <View
                                style={{
                                    borderBottomColor: 'tomato',
                                    borderBottomWidth: 1,
                                    marginTop: 16,
                                }}
                            />

                            <View style={styles.container}>
                                <MapView
                                    style={{width: '100%', height: 400}}
                                    mapType="none"
                                    maxZoomLevel={17}
                                    minZoomLevel={7}
                                    region={position}
                                    loadingEnabled={true}
                                    showsUserLocation={true}
                                    provider={PROVIDER_GOOGLE}
                                    scrollEnabled={coordinateGetted}
                                    rotateEnabled={coordinateGetted}
                                    zoomEnabled={coordinateGetted}
                                    pitchEnabled={coordinateGetted}
                                    // showsMyLocationButton={true}
                                    onRegionChangeComplete={(e) => {
                                        if (coordinateGetted) {
                                            // setStartLatitude(e.startLatitude);
                                            // setStartLongitude(e.startLongitude);
                                            setPosition({
                                                    latitude: e.latitude,
                                                    longitude: e.longitude,
                                                    latitudeDelta: e.latitudeDelta,
                                                    longitudeDelta: e.longitudeDelta
                                                }
                                            );
                                        }
                                    }}
                                >
                                    <UrlTile
                                        urlTemplate={'https://wmts.geo.admin.ch/1.0.0/ch.swisstopo.pixelkarte-grau/default/current/3857/{z}/{x}/{y}.jpeg'}
                                        zIndex={1}
                                    />
                                    <UrlTile
                                        urlTemplate='https://wmts.geo.admin.ch/1.0.0/ch.swisstopo.swisstlm3d-wanderwege/default/current/3857/{z}/{x}/{y}.png'
                                        zIndex={2}
                                    />
                                    <Marker coordinate={{
                                        latitude: startLatitude,
                                        longitude: startLongitude
                                    }}
                                            zIndex={10}
                                            draggable={true}
                                            pinColor="green"
                                            title={t("_startPoint")}
                                            onDragEnd={(e) => {
                                                setStartLatitude(e.nativeEvent.coordinate.latitude);
                                                setStartLongitude(e.nativeEvent.coordinate.longitude);
                                            }}
                                    />
                                    <Marker coordinate={{
                                        latitude: endLatitude,
                                        longitude: endLongitude
                                    }}
                                            zIndex={10}
                                            draggable={true}
                                            pinColor="red"
                                            title={t("_endPoint")}
                                            onDragEnd={(e) => {
                                                setEndLatitude(e.nativeEvent.coordinate.latitude);
                                                setEndLongitude(e.nativeEvent.coordinate.longitude);
                                            }}
                                    />
                                </MapView>
                                {/*<Icon name="add-circle-outline" style={{position: "absolute", color: 'mediumslateblue'}}*/}
                                {/*      size={60}/>*/}
                            </View>
                            {position.latitudeDelta < latitudeDeltaZoomMin && position.longitudeDelta < longitudeDeltaZoomMin
                                ? <Text></Text>
                                : <Text style={{fontSize: 10, color: 'red'}}>{t("_zoomRequired")}</Text>
                            }

                            {/*DATE MANAGEMENT*/}
                            <Text style={styles.formTitle}>{t("_date")}</Text>
                            <DatePicker
                                style={{width: 200, marginBottom: 10}}
                                mode="date"
                                date={sodDate}
                                placeholder="select date"
                                format="DD-MM-YYYY"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                disabled
                                customStyles={{
                                    dateIcon: {
                                        position: 'absolute',
                                        left: 0,
                                        top: 4,
                                        marginLeft: 0
                                    },
                                    dateInput: {
                                        marginLeft: 36
                                    }
                                }}
                                value={values.sodDate}
                                onValueChange={handleChange('sodDate')}
                                onDateChange={(date) => {
                                    setSodDate(date);
                                }}
                            />

                            {/*SUBMIT BUTTON MANAGEMENT*/}
                            {/*Submit button active when all of these are set :
                                - coordinates
                                - levels
                                - image
                                - position accuracy (zoom)
                             */}
                            {
                                startLatitude != "0"
                                && startLongitude != "0"
                                && endLatitude != "0"
                                && endLongitude != "0"
                                && riskLevel != 0
                                && technicalLevel != 0
                                && image
                                && coordinateGetted
                                //Don't forget to change in MySodDetails
                                && position.latitudeDelta < latitudeDeltaZoomMin
                                && position.longitudeDelta < longitudeDeltaZoomMin
                                    ? <TouchableOpacity onPress={handleSubmit}>
                                        <Button mode="contained" color="tomato"
                                                style={styles.button}>{t("_submit")}</Button>
                                    </TouchableOpacity>

                                    : <Button mode="contained" color="tomato" disabled={true}
                                              style={styles.button}>{t("_submit")}</Button>
                            }
                        </ScrollView>
                    )}
                </Formik>
            </View>
        </View>
    );

    //Render the levels
    function renderLevels(level, type) {
        switch (level) {
            case 1:
                return <Text>{type} : {t("_levelOne")}</Text>;
            case 2:
                return <Text>{type} : {t("_levelTwo")}</Text>;
            case 3:
                return <Text>{type} : {t("_levelThree")}</Text>;
            case 4:
                return <Text>{type} : {t("_levelFour")}</Text>;
            case 5:
                return <Text>{type} : {t("_levelFive")}</Text>;
            default:
                return <Text>{type} : </Text>;
        }
    }
}

const inputTheme = {
    colors: {
        primary: 'tomato',
        underlineColor: 'transparent',

    }
};

const styles = StyleSheet.create({
    input: {
        backgroundColor: 'white',
        marginTop: 15,
    },
    container: {
        flex: 1,
        display: "flex",
        justifyContent: 'center',
        alignItems: 'center',
    },
    content: {
        padding: 16,
    },
    button: {
        height: 38,
    },
    coordinates: {
        width: '40%',
        height: 20,
        marginBottom: 10,
        marginTop: 10
    },
    formTitle: {
        marginTop: 15,
        marginBottom: 5,
        fontSize: 18,
        // textAlign: 'center',
    },
    touchableZoneImage: {
        width: '100%',
        height: 200,
        backgroundColor: 'white',
        justifyContent: 'center'
    },
    imageSize: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain',
    },
    imageContainer: {
        width: '100%',
        height: 200,
        alignItems: 'center',
        justifyContent: 'center',
        alignContent: 'center',
    }
});
