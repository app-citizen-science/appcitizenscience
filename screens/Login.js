import React, {useState} from 'react';
import {
    ActivityIndicator,
    Alert,
    Image,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    View,
    KeyboardAvoidingView,
    Platform
} from 'react-native';
import firebase, {db} from '../database/firebase';
import {Button} from 'react-native-elements';
import {t} from "../languages";
import {locale} from "expo-localization";
import {useIsConnected} from "react-native-offline";

export default function Login({navigation}) {
    const firestore_ref = db.collection('users')
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    let isConnected = useIsConnected();

    const userLogin = () => {
        if(!isConnected) {
            //If the user is not connected to internet alert him
            Alert.alert(t('_loginOfflineTitle'),t('_loginOffline'));
        }else{
            if (email === '' && password === '') {
                Alert.alert(t("_enterDetails"))
            } else {
                setIsLoading(true)
                firebase.auth().languageCode = locale.substring(0, 2);
                firebase
                    .auth()
                    .signInWithEmailAndPassword(email, password)
                    .then((res) => {
                        //Check if email is verified
                        if (!res.user.emailVerified) {
                            //navigation.navigate('Login');
                            firebase.auth().signOut();
                            //If not display an alert message
                            Alert.alert(
                                t('_emailVerificationTitle'),
                                t('_emailVerificationMessage'),
                                [
                                    {
                                        text: t('_emailSendBack'),
                                        onPress: () => res.user.sendEmailVerification()
                                    },
                                    {
                                        text: t('_ok'),
                                        onPress: () => null,
                                    }
                                ]);
                        } else {
                            const docRef = firestore_ref.doc(res.user.uid)
                            docRef.get().then((doc) => {
                                try {
                                    if (doc.data().blacklist === true) {
                                        Alert.alert(t("_signinFailed"));
                                        //navigation.navigate('Login');
                                        firebase.auth().signOut()
                                    } else {
                                        setIsLoading(true)
                                        setEmail('')
                                        setPassword('')
                                        //navigation.navigate('BottomTabsNavigator');
                                    }
                                } catch (error) {
                                    console.log(error.message)
                                }
                            })
                        }
                    })
                    .catch((error) => {
                        console.log(error.message)
                        Alert.alert(t("_badlyFormatted"))
                        setIsLoading(false)
                        setEmail('')
                        setPassword('')
                    })
            }
        }
    }

    if (isLoading) {
        return (
            <View style={styles.preloader}>
                <ActivityIndicator size="large" color="tomato"/>
            </View>
        )
    }
    return (
            <ScrollView style={styles.container}>

                <Image source={require('../assets/icon_santour.png')} style={styles.appIcon}/>
                <TextInput
                    style={styles.inputStyle}
                    placeholder={t("_email")}
                    value={email}
                    onChangeText={email => setEmail(email)}
                    keyboardType="email-address"
                    autoCapitalize='none'
                    returnKeyType="done"
                />
                <TextInput
                    style={styles.inputStyle}
                    placeholder={t("_password")}
                    value={password}
                    onChangeText={password => setPassword(password)}
                    maxLength={15}
                    secureTextEntry={true}
                    returnKeyType="done"
                />
                <Button
                    type="solid"
                    buttonStyle={{backgroundColor: 'tomato', marginTop: 50}}
                    title={t("_login")}
                    onPress={() => userLogin()}
                />
                <Text
                    style={styles.loginText}
                    onPress={() => navigation.navigate('Signup')}>
                    {t("_createNewAccount")}
                </Text>
            </ScrollView>
    );
}

const styles = StyleSheet.create({
    appIcon: {
        width: 150,
        height: 150,
        alignSelf: 'center',
        borderRadius: 10,
        marginTop: 50,
        marginBottom: 70
    },
    container: {
        flex: 1,
        display: "flex",
        flexDirection: "column",
        padding: 35,
        paddingTop: 0,
        backgroundColor: '#fff'
    },
    inputStyle: {
        width: '100%',
        marginBottom: 15,
        alignSelf: "center",
        borderColor: "#ccc",
        fontSize: 14,
        padding: 20,
        borderRadius: 10,
        borderWidth: 1,
    },
    loginText: {
        color: 'tomato',
        marginTop: 25,
        textAlign: 'center'
    },
    preloader: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff'
    }
});
