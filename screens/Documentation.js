import React from 'react';
import {StyleSheet, View, Text, Image, ScrollView} from 'react-native';
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import {t} from "../languages";
import PDFReader from 'rn-pdf-reader-js';
import Constants from "expo-constants";


export default function Documentation({navigation}){
    return (
        <View style={styles.container}>
            <View>
                <View style={styles.textContainer}>
                    <Text style={styles.textStyle}>{t('_documentationText')}</Text>
                </View>
                <View style={styles.textContainer}>
                    <Text
                        style={styles.textLinkStyle}
                        onPress={() => navigation.navigate('Risk')}>
                        {t("_riskDocTitle")}
                    </Text>
                </View>
                <View style={styles.textContainer}>
                    <Text
                        style={styles.textLinkStyle}
                        onPress={() => navigation.navigate('Technicality')}>
                        {t("_technicalDocTitle")}
                    </Text>
                </View>


            </View>
        </View>
    )
}

const styles = StyleSheet.create({

    textContainer: {
        marginBottom: 20,
        marginHorizontal: 30,
        backgroundColor: 'white',
        borderRadius: 10
    },
    textStyle: {
        padding: 20,
        color: 'grey'
    },
    textLinkStyle: {
        padding: 20,
        color: 'tomato'
    },
    container: {
        flex: 1,
        marginTop: "10%"
    }
});



