import React, {useState} from 'react';
import {StyleSheet, Text, View, TextInput, Alert, ActivityIndicator, Switch, ScrollView} from 'react-native';
import {Button, Input} from 'react-native-elements';
import firebase from '../database/firebase';
import {t} from "../languages";
import {Formik} from "formik";
import {db} from "../database/firebase";
import * as Yup from 'yup';
import {FormFieldError} from "../components/FormFieldError";
import {locale} from "expo-localization";
import {useIsConnected} from "react-native-offline";


export default function Signup({navigation}) {
    const firestore_ref = db.collection('users');
    const [isLoading, setIsLoading] = useState(false);
    const [legalAccepted, setLegalAccepted] = useState(false);

    let isConnected = useIsConnected();

    //Yup validation schema
    const SignUpSchema = Yup.object().shape({
        displayName: Yup.string()
            .min(4, t('_displayNameMin'))
            .max(30, t('_displayNameMax'))
            .required(t('_displayNameRequired')),
        email: Yup.string()
            .email(t('_invalidEmail'))
            .required(t('_emailRequired')),
        password: Yup.string()
            .min(6, t('_passwordMin'))
            .max(30, t('_passwordMax'))
            .required(t('_passwordRequired')),
        passwordCopy: Yup.string()
            .oneOf([Yup.ref('password')], t('_passwordsNotMatch'))
            .required(t('_passwordConfirmationRequired'))
    });

    const registerUser = (values) => {
        if(!isConnected) {
            //If the user is not connected to internet alert him
            Alert.alert(t('_loginOfflineTitle'),t('_signinOffline'));
        }else {
            let emailToSend = values.email.toLowerCase();

            setIsLoading(true);

            //Remove spaces if present at the beginning or the end of the email
            if (values.email.charAt(values.email.length - 1) === ' ') {
                emailToSend = values.email.substring(0, values.email.length - 1).toLowerCase();
            }
            if (emailToSend.charAt(0) === ' ') {
                emailToSend = emailToSend.substring(1, emailToSend.length).toLowerCase();
            }
            firebase.auth().languageCode = locale.substring(0, 2);
            firebase
                .auth()
                .createUserWithEmailAndPassword(emailToSend, values.password)
                .then((res) => {

                    //Add user in firestore
                    res.user.updateProfile({
                        displayName: values.displayName
                    }).catch(error => console.log(error.message));
                    const userdata = firestore_ref.doc(res.user.uid)
                    userdata.set({
                        attributed_users: [],
                        blacklist: false,
                        username: values.displayName,
                        email: emailToSend,
                        expert: "NA",
                        role: 'Beginner'
                    }).catch(error => console.log(error.message));
                    setIsLoading(false)

                    //Send verification email
                    res.user.sendEmailVerification()
                        .then(() => Alert.alert(t('_emailVerificationTitle'), t('_emailVerificationMessage')))
                        .catch(error => console.log(error.message));

                    firebase.auth().signOut();
                    navigation.navigate('Login');
                })
                .catch((error) => {
                    Alert.alert(error.message)
                    setIsLoading(false)
                })
        }
    }

    if (isLoading) {
        return (
            <View style={styles.preloader}>
                <ActivityIndicator size="large" color="tomato"/>
            </View>
        )
    }
    return (
        <Formik
            initialValues={{
                displayName: '',
                email: '',
                password: '',
                passwordCopy: ''
            }}
            onSubmit={(values) => registerUser(values)}
            validationSchema={SignUpSchema}
            validateOnBlur={true}
            validateOnChange={false}
        >
            {({
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  values,
                  errors,
                  touched,
                  isValid
              }) => (
                <ScrollView style={styles.container}>
                    <TextInput
                        style={styles.inputStyle}
                        placeholder={t("_name")}
                        value={values.displayName}
                        onChangeText={handleChange('displayName')}
                        onBlur={handleBlur('displayName')}
                        returnKeyType="done"
                    />
                    {(errors.displayName && touched.displayName) &&
                    <FormFieldError text={errors.displayName}/>
                    }
                    <TextInput
                        style={styles.inputStyle}
                        placeholder={t("_email")}
                        value={values.email}
                        onChangeText={handleChange('email')}
                        autoCapitalize='none'
                        onBlur={handleBlur('email')}
                        keyboardType="email-address"
                        returnKeyType="done"
                    />
                    {(errors.email && touched.email) &&
                    <FormFieldError text={errors.email}/>
                    }
                    <TextInput
                        style={styles.inputStyle}
                        placeholder={t("_password")}
                        value={values.password}
                        onChangeText={handleChange('password')}
                        onBlur={handleBlur('password')}
                        maxLength={30}
                        secureTextEntry={true}
                        returnKeyType="done"
                    />
                    {(errors.password && touched.password) &&
                    <FormFieldError text={errors.password}/>
                    }
                    <TextInput
                        style={styles.inputStyle}
                        placeholder={t("_passwordConfirmation")}
                        value={values.passwordCopy}
                        onChangeText={handleChange('passwordCopy')}
                        onBlur={handleBlur('passwordCopy')}
                        maxLength={30}
                        secureTextEntry={true}
                        returnKeyType="done"
                    />
                    {(errors.passwordCopy && touched.passwordCopy) &&
                    <FormFieldError text={errors.passwordCopy}/>
                    }
                    <Text
                        style={styles.loginText}
                        onPress={() => navigation.navigate('SignupLegal')}>
                        {t("_privacyPolicyAndTermsConditions")}
                    </Text>
                    <View style={styles.legal}>
                        <Text style={{marginRight: 10}}>{t('_legalAcceptance')}</Text>
                        <Switch
                            value={legalAccepted}
                            onValueChange={() => setLegalAccepted(!legalAccepted)}
                        />
                    </View>
                    <Button
                        type="solid"
                        buttonStyle={{backgroundColor: 'tomato'}}
                        title={t("_signUp")}
                        onPress={handleSubmit}
                        disabled={!legalAccepted}
                    />
                    <Text
                        style={styles.loginText}
                        onPress={() => navigation.navigate('Login')}>
                        {t("_alreadyRegistered")}
                    </Text>
                </ScrollView>
            )}

        </Formik>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        display: "flex",
        flexDirection: "column",
        padding: 35,
        backgroundColor: '#fff'
    },
    legal: {
        marginBottom: 40,
        marginTop: 40,
        alignItems: "center",
        flexDirection: "row",
        width: "95%"
    },
    signUpTitle:{
        fontSize: 26,
        color: 'tomato',
        marginBottom: 20
    },
    inputStyle: {
        width: '100%',
        marginTop: 15,
        alignSelf: "center",
        borderColor: "#ccc",
        fontSize: 14,
        padding: 15,
        borderRadius: 10,
        borderWidth: 1,
    },
    loginText: {
        color: 'tomato',
        marginTop: 25,
        textAlign: 'center'
    },
    preloader: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff'
    }
});
