import React from 'react';
import { StyleSheet, View, Text} from 'react-native';
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import {t} from "../languages";


export default function Settings(){
    return (

        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', minHeight: '10%'}}>
            <FontAwesome5 name="hiking" size={90} color="tomato"/>
            <Text style={{color: 'tomato', marginTop: 10, fontWeight: 'bold'}}>{t('_inDevelopment')}</Text>
        </View>

    )
}


