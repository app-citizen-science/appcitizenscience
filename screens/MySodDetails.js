import * as React from 'react';
import {
    Alert,
    ActivityIndicator,
    Text,
    View,
    ScrollView,
    Image,
    StyleSheet,
    Keyboard, LogBox, SafeAreaView
} from "react-native";
import {useContext, useEffect, useState} from "react";
import {deleteSod, getSod, updatePodSodImageUrl, updateSod} from "../database/firestore_requests";
import {mapDocToSod} from "../database/maping_methods";
import {Button, TextInput} from "react-native-paper";
import DatePicker from "react-native-datepicker";
import ActionButton from "react-native-action-button";
import Icon from 'react-native-vector-icons/Ionicons';
import {t} from "../languages";
import {Formik} from "formik";
import * as yup from "yup";
import {deleteSodImage, uploadPodSodImage} from "../database/storage_requests";
import * as ImagePicker from "expo-image-picker";
import MapView, {Marker, PROVIDER_GOOGLE, UrlTile} from "react-native-maps";
import firebase from "firebase";
import {latitudeDeltaZoomMin, longitudeDeltaZoomMin} from "./NewPod";
import ConnectionManagement from "../components/ConnectionManagement";
import {UserContext} from "../utils/UserContext";
import {TouchableOpacity} from 'react-native-gesture-handler';

/*
    Class to display the Sod details (display, edit and delete SOD)
*/

export function MySodDetails({route, navigation}) {
    let [sod, setSod] = useState();
    const {doc_id, from_map, riskLvl, technicalLvl} = route.params;

    const [riskLevel, setRiskLevel] = useState(riskLvl);
    const [technicalLevel, setTechnicalLevel] = useState(technicalLvl);
    const [disabled, setDisabled] = useState(true);
    const [editable, setEditable] = useState(false);
    const [image, setImage] = useState(null);

    const [startLatitude, setStartLatitude] = useState(0);
    const [startLongitude, setStartLongitude] = useState(0);
    const [endLatitude, setEndLatitude] = useState(0);
    const [endLongitude, setEndLongitude] = useState(0);

    const [status, setStatus] = useState(null);
    let UserType = useContext(UserContext);

    let [position, setPosition] = useState({
        latitude: 0,
        longitude: 0,
        latitudeDelta: 3,
        longitudeDelta: 3
    });

    const levelColor = {
        one: 'green',
        two: 'blue',
        three: 'orange',
        four: 'red',
        five: 'black'
    }

    useEffect(() =>{
        //Check the user role
        if (UserType.user.role === 'Beginner'){
            setStatus('toValidate');
        }else{
            setStatus('validated');
        }

        //Check if it's coming from the map (edit directly)
        if (from_map) {
            navigation.setOptions({
                title: t("_sodsDetailsEditOn"),
            });
            setEditable(true);
            setDisabled(false);
        }
    })

    useEffect(() => {
        //Get the SOD
        const unsubscribe = getSod({
            next: documentSnapshot => {
                if (documentSnapshot.data() !== undefined) {
                    let sodFirestore = mapDocToSod(documentSnapshot);
                    setSod(sodFirestore);
                    calculateCenterPosition(sodFirestore);
                    setStartLatitude(sodFirestore.geo_position_start.latitude);
                    setStartLongitude(sodFirestore.geo_position_start.longitude);
                    setEndLatitude(sodFirestore.geo_position_end.latitude);
                    setEndLongitude(sodFirestore.geo_position_end.longitude);
                    navigation.setOptions({
                        title: t("_sodsDetailsEditOff") + " " + sodFirestore.sod_id,
                    });
                } else {
                    setSod(undefined);
                }
            },
            error: (error) => {
                setSod(undefined);
            }
        }, doc_id);

        return () => unsubscribe();
    }, [setSod]);

    function calculateCenterPosition(sod) {
        //This function calculate the latitudeDelta and longitudeDelta according to the distance of the sod extremities
        //The idea of this function is to try to display the two sod extremities on the screen
        const longitudeStart = sod.geo_position_start.longitude;
        const latitudeStart = sod.geo_position_start.latitude;
        const longitudeEnd = sod.geo_position_end.longitude;
        const latitudeEnd = sod.geo_position_end.latitude;

        //Calculate difference between the two longitude
        let distanceLongitudes = longitudeEnd - longitudeStart;
        if (distanceLongitudes < 0) {
            distanceLongitudes = distanceLongitudes * (-1);
        }

        //Calculate difference between the two latitudes
        let distanceLatitudes = latitudeEnd - latitudeStart;
        if (distanceLatitudes < 0) {
            distanceLatitudes = distanceLatitudes * (-1);
        }

        setPosition({
            latitude: (sod.geo_position_start.latitude+sod.geo_position_end.latitude)/2,
            longitude: (sod.geo_position_start.longitude+sod.geo_position_end.longitude)/2,
            latitudeDelta: distanceLatitudes * 2,
            longitudeDelta: distanceLongitudes * 2
        });
    }

    //Image picker from Library
    const pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            aspect: [4, 3],
            quality: 1,
        });

        if (!result.cancelled) {
            setImage(result.uri);
        }
    };

    //Camera picker
    const pickCamera = async () => {
        let result = await ImagePicker.launchCameraAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            aspect: [4, 3],
            quality: 1,
        });

        if (!result.cancelled) {
            setImage(result.uri);
        }
    };

    return (
        <View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'stretch'}}>
            <View style={{position:"absolute", top:0, zIndex:200, width:'100%'}}>
                <ConnectionManagement/>
            </View>
            {
                sod === undefined ? (
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <ActivityIndicator size='large'/>
                    </View>
                )
                :
                editable
                    ? editOnView()
                    : editOffView()
            }

            {
                editable
                    ? <View/>
                    : <ActionButton buttonColor="rgba(231,76,60,1)"
                                    degrees={90}
                                    renderIcon={() =>
                                        <Icon name="ellipsis-horizontal-outline" style={styles.actionButtonIcon}/>
                                    }>
                        <ActionButton.Item buttonColor='rgba(240, 52, 52, 1)' title={t("_deleteSod")} onPress={async () => {
                            Alert.alert(
                                t("_deleteSod"),
                                t("_confirmationMessage"),
                                [
                                    {
                                        text: t("_cancel"),
                                        onPress: () => console.log('Canceled deletion of sod'),
                                        style: 'cancel',
                                    },
                                    {
                                        text: t("_delete"), onPress: async () => {
                                            await navigation.goBack();
                                            await deleteSodImage(sod, doc_id).then(() => {
                                                    deleteSod(doc_id);
                                                }
                                            );
                                        }
                                    },
                                ],
                                {cancelable: false},
                            );
                        }}>
                            <Icon name="trash-outline" style={styles.actionButtonIcon}/>
                        </ActionButton.Item>

                        <ActionButton.Item buttonColor='rgba(34, 167, 240, 1)' title={t("_editSod")} onPress={() => {
                            navigation.setOptions({
                                title: t("_sodsDetailsEditOn") + " " + sod.sod_id,
                            })
                            setImage(null);
                            setRiskLevel(sod.level_risk);
                            setTechnicalLevel(sod.level_technical);
                            setEditable(true);
                            setDisabled(false);
                        }}>
                            <Icon name="md-create" style={styles.actionButtonIcon}/>
                        </ActionButton.Item>
                    </ActionButton>
            }


        </View>
    );

    //Yup login validation
    const formValidationSchema = yup.object().shape({
        sodName: yup
            .string()
            .required(t("_sodNameRequired")),
    })

    //View to edit the sod
    function editOnView() {
        return (
            <View style={styles.content}>
                <Formik
                    validationSchema={formValidationSchema}
                    initialValues={{
                        sodName: sod.name,
                        sodDescription: sod.description,
                        techniqueLvl: sod.level_technical,
                        riskLvl: sod.level_risk,
                        sodDate: ''
                    }}
                    onSubmit={async values => {
                        await updateSod(
                            doc_id,
                            values,
                            riskLevel,
                            technicalLevel,
                            new firebase.firestore.GeoPoint(startLatitude, startLongitude),
                            new firebase.firestore.GeoPoint(endLatitude, endLongitude),
                            status
                        );

                        //IMAGE MANAGEMENT
                        if (image) {
                            await deleteSodImage(sod, doc_id);

                            uploadPodSodImage(image, doc_id, "sods/").then(
                                (value2) => {
                                    updatePodSodImageUrl(doc_id, value2, "segmentsOfDifficulty");
                                }
                            );
                            console.log('image has been changed');
                        }

                        navigation.setOptions({
                            title: t("_sodsDetailsEditOff") + " " + sod.sod_id,
                        })

                        if(from_map){
                            navigation.navigate('Map');
                        }else {
                            setEditable(false);
                            setDisabled(true);
                        }

                        Keyboard.dismiss();
                    }
                    }>
                    {({handleChange, errors, handleSubmit, values}) => (
                        <ScrollView>
                            <TouchableOpacity style={styles.touchableZoneImage}
                                              onPress={() => {
                                                  Alert.alert(
                                                      t("_addImageAlertTitle"),
                                                      t("_addImageAlertMessage"),
                                                      [
                                                          {
                                                              text: t("_cameraRoll"),
                                                              onPress: () => (pickImage())
                                                          },
                                                          {
                                                              text: t("_takePhoto"),
                                                              onPress: () => (pickCamera())
                                                          },
                                                          {
                                                              text: t("_cancel"),
                                                              onPress: () => {
                                                                  console.log('Pick canceled')
                                                              },
                                                              style: "cancel"
                                                          }
                                                      ]
                                                  )
                                              }}
                            >
                                {/*<Icon name="camera" style={{color:'tomato'}} size={100}/>*/}
                                {/*<Image source={{uri: sod.img_download_url}} style={{width: 400, height: 200}}/>*/}
                                <View>
                                    {
                                        image
                                            ?
                                            <Image source={{uri: image, cache: 'force-cache'}}
                                                   style={styles.imageSize}/>
                                            :
                                            <Image source={{uri: sod.img_download_url, cache: 'force-cache'}}
                                                   style={styles.imageSize}/>
                                    }
                                </View>
                            </TouchableOpacity>
                            {/*Image*/}
                            {/*SOD NAME*/}
                            <TextInput
                                name="sodName"
                                style={styles.input}
                                theme={inputTheme}
                                onChangeText={handleChange('sodName')}
                                value={values.sodName}
                                label={t("_sodName")}
                                underlineColor="tomato"
                                selectionColor="tomato"
                                underlineColorAndroid="tomato"
                                keyboardType="default"
                                returnKeyType="done"
                                blurOnSubmit={true}
                                onSubmitEditing={()=>{Keyboard.dismiss()}}
                            />
                            {errors.sodName &&
                            <Text style={{fontSize: 10, color: 'red'}}>{errors.sodName}</Text>
                            }

                            {/*SOD DESCRIPTION*/}
                            <TextInput
                                style={styles.input}
                                theme={inputTheme}
                                onChangeText={handleChange('sodDescription')}
                                value={values.sodDescription}
                                label={t("_sodDescription")}
                                underlineColor="tomato"
                                selectionColor="tomato"
                                multiline={true}
                                keyboardType="default"
                                returnKeyType="done"
                                blurOnSubmit={true}
                                onSubmitEditing={()=>{Keyboard.dismiss()}}
                            />

                            {/*RISK LEVEL MANAGEMENT*/}
                            {
                                sodRiskLevel(riskLevel)
                            }

                            {/*Technical Level*/}
                            {
                                sodTechnicalLevel(technicalLevel)
                            }

                            {/*Sod map*/}
                            {
                                sodMap(true)
                            }
                            {
                                position.latitudeDelta < latitudeDeltaZoomMin && position.longitudeDelta < longitudeDeltaZoomMin
                                    ? <Text></Text>
                                    : <Text style={{fontSize: 10, color: 'red'}}>{t("_zoomRequired")}</Text>
                            }

                            {/*DATE MANAGEMENT*/}
                            {/* Date */}
                            {
                                sodDate()
                            }

                            {/*SUBMIT BUTTON MANAGEMENT*/}
                            {/*Submit button active when all of these are set :
                                - levels
                                - position accuracy (zoom)
                             */}
                            {
                                riskLevel != 0
                                && technicalLevel != 0
                                //Don't forget to change in NewSod
                                && position.latitudeDelta < latitudeDeltaZoomMin
                                && position.longitudeDelta < longitudeDeltaZoomMin
                                    ?
                                    <View style={styles.levelViewStyle}>
                                        {
                                            cancelBtn()
                                        }
                                        <View style={styles.viewFiftyFifty}>
                                            <TouchableOpacity onPress={handleSubmit}>
                                                <Button mode="contained" color="green" style={styles.button}>{t("_submit")}</Button>
                                            </TouchableOpacity>

                                        </View>
                                    </View>
                                    :
                                    <View style={styles.levelViewStyle}>
                                        {
                                            cancelBtn()
                                        }
                                        <View style={styles.viewFiftyFifty}>
                                            <Button mode="contained" color="green" disabled={true}
                                                    style={styles.button}>{t("_submit")}</Button>
                                        </View>
                                    </View>
                            }
                        </ScrollView>
                    )}
                </Formik>
            </View>
        );
    }

    //View to display the sod
    function editOffView() {
        return (
            <View style={styles.content}>
                <ScrollView>
                    {/*Image*/}
                    <View style={styles.imageContainer}>
                        <Image source={{uri: sod.img_download_url}} style={styles.imageSize}/>
                    </View>
                    {/*Name*/}
                    <TextInput
                        style={styles.input}
                        theme={inputTheme}
                        value={sod.name}
                        label={t("_sodName")}
                        underlineColor="tomato"
                        selectionColor="tomato"
                        editable={editable}
                    />
                    {/*Description*/}
                    <TextInput
                        style={styles.input}
                        theme={inputTheme}
                        value={sod.description}
                        label={t("_sodDescription")}
                        underlineColor="tomato"
                        selectionColor="tomato"
                        editable={editable}
                        multiline={true}
                    />

                    {/*Risk Level*/}
                    {
                        sodRiskLevel(sod.level_risk)
                    }

                    {/*Technical Level*/}
                    {
                        sodTechnicalLevel(sod.level_technical)
                    }

                    {/*Map*/}
                    {
                        sodMap(false)
                    }

                    {/* Date */}
                    {
                        sodDate()
                    }

                </ScrollView>
            </View>
        );
    }

    // DISPLAY THE MAP W/MARKER ACCORDING TO EDITMODE
    function sodMap(editMode) {
        return <View>
            {
                editMode
                    ? <SafeAreaView style={styles.container}>
                        <MapView
                            style={styles.mapView}
                            mapType="none"
                            maxZoomLevel={17}
                            minZoomLevel={7}
                            region={position}
                            loadingEnabled={true}
                            showsUserLocation={true}
                            provider={PROVIDER_GOOGLE}
                            onRegionChangeComplete={(e) => {
                                setPosition({
                                        latitude: e.latitude,
                                        longitude: e.longitude,
                                        latitudeDelta: e.latitudeDelta,
                                        longitudeDelta: e.longitudeDelta
                                    }
                                );
                            }}
                        >
                            <UrlTile
                                urlTemplate={'https://wmts.geo.admin.ch/1.0.0/ch.swisstopo.pixelkarte-grau/default/current/3857/{z}/{x}/{y}.jpeg'}
                                zIndex={1}
                            />
                            <UrlTile
                                urlTemplate='https://wmts.geo.admin.ch/1.0.0/ch.swisstopo.swisstlm3d-wanderwege/default/current/3857/{z}/{x}/{y}.png'
                                zIndex={2}
                            />
                            <Marker
                                coordinate={{
                                    latitude: sod.geo_position_start.latitude,
                                    longitude: sod.geo_position_start.longitude
                                }}
                                draggable={true}
                                pinColor="green"
                                zIndex={10}
                                title={sod.name}
                                onDragEnd={(e) => {
                                    setStartLatitude(e.nativeEvent.coordinate.latitude);
                                    setStartLongitude(e.nativeEvent.coordinate.longitude);
                                }}
                            />

                            <Marker
                                coordinate={{
                                    latitude: sod.geo_position_end.latitude,
                                    longitude: sod.geo_position_end.longitude
                                }}
                                zIndex={10}
                                draggable={true}
                                pinColor="red"
                                title={sod.name}
                                onDragEnd={(e) => {
                                    setEndLatitude(e.nativeEvent.coordinate.latitude);
                                    setEndLongitude(e.nativeEvent.coordinate.longitude);
                                }}
                            />

                        </MapView>

                    </SafeAreaView>
                    : <MapView
                        style={styles.mapView}
                        mapType="none"
                        maxZoomLevel={17}
                        minZoomLevel={7}
                        region={position}
                        loadingEnabled={editMode}
                        showsUserLocation={editMode}
                        provider={PROVIDER_GOOGLE}
                        showsMyLocationButton={editMode}
                        pitchEnabled={false}
                        rotateEnabled={false}
                        scrollEnabled={false}
                        zoomEnabled={true}
                    >
                        <UrlTile
                            urlTemplate={'https://wmts.geo.admin.ch/1.0.0/ch.swisstopo.pixelkarte-grau/default/current/3857/{z}/{x}/{y}.jpeg'}
                            zIndex={1}
                        />
                        <UrlTile
                            urlTemplate='https://wmts.geo.admin.ch/1.0.0/ch.swisstopo.swisstlm3d-wanderwege/default/current/3857/{z}/{x}/{y}.png'
                            zIndex={2}
                        />
                        <Marker
                            coordinate={{
                                latitude: sod.geo_position_start.latitude,
                                longitude: sod.geo_position_start.longitude
                            }}
                            pinColor="green"
                            zIndex={10}
                            title={sod.name}
                            description={sod.description}
                        />

                        <Marker
                            coordinate={{
                                latitude: sod.geo_position_end.latitude,
                                longitude: sod.geo_position_end.longitude
                            }}
                            pinColor="end"
                            zIndex={10}
                            title={sod.name}
                            description={sod.description}
                        />
                    </MapView>

            }
        </View>
    }

    // DISPLAY THE SOD RISK LEVEL
    function sodRiskLevel(riskLvl) {
        return <View>
            <Text style={styles.formTitle}>
                {
                    renderLevel(riskLvl, t("_riskLevel"))
                }
            </Text>
            <View style={styles.levelViewStyle}>
                <View style={{height: 50}}>
                    {
                        riskLvl != 1
                            ? <TouchableOpacity
                                onPress={() => {
                                    setRiskLevel(1);
                                }}
                                disabled={disabled}
                            >
                                <Button mode="contained" color="grey"
                                        disabled={disabled}>
                                    1
                                </Button>
                            </TouchableOpacity>
                            : <Button mode="contained" color={levelColor.one}>
                                1
                            </Button>
                    }
                </View>
                <View style={{height: 50}}>
                    {
                        riskLvl != 2
                            ? <TouchableOpacity
                                onPress={() => {
                                    setRiskLevel(2);
                                }}
                                disabled={disabled}
                            >
                                <Button mode="contained" color="grey"
                                        disabled={disabled}>
                                    2
                                </Button>
                            </TouchableOpacity>
                            : <Button mode="contained" color={levelColor.two}>
                                2
                            </Button>
                    }
                </View>
                <View style={{height: 50}}>
                    {
                        riskLvl != 3
                            ? <TouchableOpacity
                                onPress={() => {
                                    setRiskLevel(3);
                                }}
                                disabled={disabled}
                            >
                                <Button mode="contained" color="grey"
                                        disabled={disabled}>
                                    3
                                </Button>
                            </TouchableOpacity>
                            : <Button mode="contained" color={levelColor.three}>
                                3
                            </Button>
                    }
                </View>
                <View style={{height: 50}}>
                    {
                        riskLvl != 4
                            ? <TouchableOpacity
                                onPress={() => {
                                    setRiskLevel(4);
                                }}
                                disabled={disabled}
                            >
                                <Button mode="contained" color="grey"
                                        disabled={disabled}>
                                    4
                                </Button>
                            </TouchableOpacity>
                            : <Button mode="contained" color={levelColor.four}>
                                4
                            </Button>
                    }
                </View>
                <View style={{height: 50}}>
                    {
                        riskLvl != 5
                            ? <TouchableOpacity
                                onPress={() => {
                                    setRiskLevel(5);
                                }}
                                disabled={disabled}
                            >
                                <Button mode="contained" color="grey"
                                        disabled={disabled}>
                                    5
                                </Button>
                            </TouchableOpacity>
                            : <Button mode="contained" color={levelColor.five}>
                                5
                            </Button>
                    }
                </View>
            </View>
        </View>
    }

    // DISPLAY THE SOD TECHNICAL LEVEL
    function sodTechnicalLevel(technicalLvl) {
        return <View>
            <Text style={styles.formTitle}>
                {
                    renderLevel(technicalLvl, t("_technicalLevel"))
                }
            </Text>
            <View style={styles.levelViewStyle}>
                <View style={{height: 50}}>
                    {
                        technicalLvl != 1
                            ? <TouchableOpacity
                                onPress={() => {
                                    setTechnicalLevel(1);
                                }}
                                disabled={disabled}
                            >
                                <Button mode="contained" color="grey"
                                        disabled={disabled}>
                                    1
                                </Button>
                            </TouchableOpacity>
                            : <Button mode="contained" color={levelColor.one}>
                                1
                            </Button>
                    }
                </View>
                <View style={{height: 50}}>
                    {
                        technicalLvl != 2
                            ? <TouchableOpacity
                                onPress={() => {
                                    setTechnicalLevel(2);
                                }}
                                disabled={disabled}
                            >
                                <Button mode="contained" color="grey"
                                        disabled={disabled}>
                                    2
                                </Button>
                            </TouchableOpacity>
                            : <Button mode="contained" color={levelColor.two}>
                                2
                            </Button>
                    }
                </View>
                <View style={{height: 50}}>
                    {
                        technicalLvl != 3
                            ? <TouchableOpacity
                                onPress={() => {
                                    setTechnicalLevel(3);
                                }}
                                disabled={disabled}
                            >
                                <Button mode="contained" color="grey"
                                        disabled={disabled}>
                                    3
                                </Button>
                            </TouchableOpacity>
                            : <Button mode="contained" color={levelColor.three}>
                                3
                            </Button>
                    }
                </View>
                <View style={{height: 50}}>
                    {
                        technicalLvl != 4
                            ? <TouchableOpacity
                                onPress={() => {
                                    setTechnicalLevel(4);
                                }}
                                disabled={disabled}
                            >
                                <Button mode="contained" color="grey"
                                        disabled={disabled}>
                                    4
                                </Button>
                            </TouchableOpacity>
                            : <Button mode="contained" color={levelColor.four}>
                                4
                            </Button>
                    }
                </View>
                <View style={{height: 50}}>
                    {
                        technicalLvl != 5
                            ? <TouchableOpacity
                                onPress={() => {
                                    setTechnicalLevel(5);
                                }}
                                disabled={disabled}
                            >
                                <Button mode="contained" color="grey"
                                        disabled={disabled}>
                                    5
                                </Button>
                            </TouchableOpacity>
                            : <Button mode="contained" color={levelColor.five}>
                                5
                            </Button>
                    }
                </View>
            </View>
        </View>
    }

    // DISPLAY THE SOD LEVEL ACCORDING TO THE LEVEL (TECHNICAL OR RISK) AND TYPE (1 TO 5)
    function renderLevel(level, type) {
        switch (level) {
            case 1:
                return <Text>{type} : {t("_levelOne")}</Text>;
            case 2:
                return <Text>{type} : {t("_levelTwo")}</Text>;
            case 3:
                return <Text>{type} : {t("_levelThree")}</Text>;
            case 4:
                return <Text>{type} : {t("_levelFour")}</Text>;
            case 5:
                return <Text>{type} : {t("_levelFive")}</Text>;
            default:
                return <Text>{type} : </Text>;
        }
    }

    // DISPLAY THE SOD DATE
    function sodDate() {
        return <View>
            <Text style={styles.formTitle}>Date</Text>
            <DatePicker
                style={{width: 200}}
                mode="date"
                date={sod.date.toDate()}
                placeholder="select date"
                format="DD-MM-YYYY"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                disabled
                customStyles={{
                    dateIcon: {
                        position: 'absolute',
                        left: 0,
                        top: 4,
                        marginLeft: 0
                    },
                    dateInput: {
                        marginLeft: 36
                    }
                }}
                value={sod.date.toDate()}
            />
        </View>
    }

    // CANCEL BUTTON
    function cancelBtn() {
        return (
            <View style={styles.viewFiftyFifty}>
                {
                    from_map
                    ? <TouchableOpacity
                            onPress={() => {
                                navigation.navigate('Map');
                            }}
                        >
                            <Button mode="contained" color="red"
                                    style={styles.button}>{t("_cancel")}</Button>
                        </TouchableOpacity>
                        : <TouchableOpacity
                            onPress={() => {
                                navigation.setOptions({
                                    title: t("_sodsDetailsEditOff") + " " + sod.sod_id,
                                })
                                setEditable(false);
                                setDisabled(true);
                                setPosition(
                                    {
                                        latitude: (sod.geo_position_start.latitude+sod.geo_position_end.latitude)/2,
                                        longitude: (sod.geo_position_start.longitude+sod.geo_position_end.longitude)/2,
                                        latitudeDelta: 0.004,
                                        longitudeDelta: 0.004
                                    }
                                );
                            }}
                        >
                            <Button mode="contained" color="red" style={styles.button}>{t("_cancel")}</Button>
                        </TouchableOpacity>
                }
            </View>
        );
    }
}

const inputTheme = {
    colors: {
        primary: 'tomato',
        underlineColor: 'transparent',
    }
};

const styles = StyleSheet.create({
    input: {
        backgroundColor: 'white',
        marginTop: 16,
    },
    container: {
        flex: 1,
        display: "flex",
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#ffffff'
    },
    content: {
        height: '100%',
        padding: 16,
    },
    button: {
        marginTop: 16,
    },
    coordinates: {width: '40%', height: 20, marginBottom: 10, marginTop: 10},
    formTitle: {
        marginTop: 10,
        marginBottom: 5,
        fontSize: 18,
    },
    actionButtonIcon: {
        fontSize: 20,
        height: 22,
        color: 'white',
    },
    headerEditButtonIcon: {
        color: 'rgba(46, 204, 113, 1)',
        marginRight: 15,
        marginBottom: 5
    },
    headerSaveButtonIcon: {
        color: 'rgba(34, 167, 240, 1)',
        marginRight: 15,
        marginBottom: 5
    },
    levelViewStyle: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    viewFiftyFifty: {
        width: '49%'
    },
    touchableZoneImage: {
        width: '100%',
        height: 200,
        backgroundColor: 'white',
        justifyContent: 'center'
    },
    imageSize: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain',
    },
    imageContainer: {
        width: '100%',
        height: 200,
        alignItems: 'center',
        justifyContent: 'center',
        alignContent: 'center',
    },
    mapView: {
        width: '100%',
        height: 400,
        marginTop: 16
    }
});
