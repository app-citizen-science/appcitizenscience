import * as React from 'react';
import {
    Alert,
    Keyboard,
    View,
    Text,
    StyleSheet,
    Image,
    ScrollView,
    ActivityIndicator,
} from "react-native";
import {Formik} from 'formik';
import {Button, TextInput} from 'react-native-paper';
import {useContext, useEffect, useState} from "react";
import * as ImagePicker from "expo-image-picker";
import DatePicker from 'react-native-datepicker';
import firebase from "firebase";
import * as yup from 'yup';
import {t} from "../languages";
import Icon from 'react-native-vector-icons/Ionicons';
import {addPod} from "../database/firestore_requests";
import MapView, {Marker, PROVIDER_GOOGLE, UrlTile} from "react-native-maps";
import {UserContext} from "../utils/UserContext";
import {useIsConnected} from "react-native-offline";
import {TouchableOpacity} from 'react-native-gesture-handler';

/*
    Class to create a new POD
*/

export const latitudeDeltaZoomMin = 0.005;
export const longitudeDeltaZoomMin = 0.0061;

export function NewPodScreen({navigation}) {

    const [image, setImage] = useState(null);
    const [podDate, setPodDate] = useState(new Date());
    const [riskLevel, setRiskLevel] = useState(0);
    const [technicalLevel, setTechnicalLevel] = useState(0);
    const [status, setStatus] = useState(null);

    let userContext = useContext(UserContext);
    let isConnected = useIsConnected();

    let [position, setPosition] = useState({
        latitude: 46.484,
        longitude: 8.1336,
        latitudeDelta: 3,
        longitudeDelta: 3
    });

    const levelColor = {
        one: 'green',
        two: 'blue',
        three: 'orange',
        four: 'red',
        five: 'black'
    }

    useEffect(() => {
        //Get the coordinates
        findCoordinates();

        //Check the user role
        if (userContext.user.role === 'Beginner') {
            setStatus('toValidate');
        } else {
            setStatus('validated');
        }

        if (userContext.user.role === 'Advanced' && !isConnected) {
            setStatus('toValidate');
        }
    }, []);

    //Image picker from Library
    const pickImage = async () => {
        await (async () => {
            if (Platform.OS !== 'web') {
                const responseLibraryPermission = await ImagePicker.requestMediaLibraryPermissionsAsync();
                if (responseLibraryPermission.status !== 'granted') {
                    alert(t("_cameraPermission"));
                }
            }
        })();

        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            aspect: [4, 3],
            quality: 1,
        });

        if (!result.cancelled) {
            setImage(result.uri);
        }
    };

    //Camera picker
    const pickCamera = async () => {
        await (async () => {
            if (Platform.OS !== 'web') {
                const responseCameraPermission = await ImagePicker.requestCameraPermissionsAsync();
                if (responseCameraPermission.status !== 'granted') {
                    alert(t("_cameraPermission"));
                }
            }
        })();

        let result = await ImagePicker.launchCameraAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            aspect: [4, 3],
            quality: 1,
        });

        if (!result.cancelled) {
            setImage(result.uri);
        }
    };

    const [latitude, setLatitude] = useState(0);
    const [longitude, setLongitude] = useState(0);
    const [coordinateGetted, setCoordinateGetted] = useState(false);

    //Method to find the user coordinates
    const findCoordinates = () => {
        navigator.geolocation.getCurrentPosition(
            position => {
                setLatitude(position.coords.latitude);
                setLongitude(position.coords.longitude);
                setPosition({
                        latitude: position.coords.latitude,
                        //+0.0000248104334 because of a little offset on
                        longitude: position.coords.longitude + 0.0000248104334,
                        latitudeDelta: 0.002,
                        longitudeDelta: 0.002
                    }
                );
                setCoordinateGetted(true);
            },
            error => Alert.alert(error.message),
            {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
        );
    };

    //Yup validation
    const formValidationSchema = yup.object().shape({
        podName: yup
            .string()
            .required(t("_podNameRequired")),
    })

    return (
        <View style={styles.container}>
            <View style={styles.content}>
                <Formik
                    validationSchema={formValidationSchema}
                    initialValues={{
                        podName: '',
                        podDescription: '',
                        podLatitude: latitude,
                        podLongitude: longitude,
                        techniqueLvl: '',
                        riskLvl: '',
                        podDate: ''
                    }}
                    onSubmit={async values => {

                        if (userContext.user.role === 'Advanced' && !isConnected) {
                            console.log("ADVANCED");
                            Alert.alert(
                                t("_newPod"),
                                t("_advancedUserNewPod"),
                                [
                                    {
                                        text: t("_ok"),
                                        onPress: () => {
                                            console.log('Pick canceled')
                                        },
                                        style: "cancel"
                                    }
                                ]
                            )
                        }

                        navigation.navigate('My POD');

                        // DATABASE REQUEST - POST/ADD POD
                        await addPod(
                            new firebase.firestore.FieldValue.serverTimestamp(),
                            values.podDescription,
                            '',
                            firebase.auth().currentUser.uid,
                            new firebase.firestore.GeoPoint(latitude, longitude),
                            '',
                            riskLevel,
                            technicalLevel,
                            values.podName,
                            '',
                            status,
                            image,
                            userContext
                        );

                        userContext.setPodsNumber(userContext.podsNumber + 1);

                        Keyboard.dismiss();
                    }
                    }>
                    {({handleChange, errors, handleSubmit, values}) => (
                        <ScrollView
                            // style={{height:'102%'}}
                        >
                            <TouchableOpacity style={styles.touchableZoneImage}
                                              onPress={() => {
                                                  Alert.alert(
                                                      t("_addImageAlertTitle"),
                                                      t("_addImageAlertMessage"),
                                                      [
                                                          {
                                                              text: t("_cameraRoll"),
                                                              onPress: () => (pickImage())
                                                          },
                                                          {
                                                              text: t("_takePhoto"),
                                                              onPress: () => (pickCamera())
                                                          },
                                                          {
                                                              text: t("_cancel"),
                                                              onPress: () => {
                                                                  console.log('Pick canceled')
                                                              },
                                                              style: "cancel"
                                                          }
                                                      ]
                                                  )
                                              }}
                            >
                                <View>
                                    {
                                        image
                                            ?
                                            <View style={styles.imageContainer}>
                                                <Image source={{uri: image, cache: 'force-cache'}}
                                                       style={styles.imageSize}/>
                                            </View>
                                            :

                                            <View style={{alignItems: 'center', justifyContent: 'center'}}>
                                                <Icon name="camera" style={{color: 'tomato'}} size={100}/>
                                                <Text>{t("_tapToAddPicture")}</Text>
                                            </View>
                                    }
                                </View>
                            </TouchableOpacity>

                            {/*POD NAME*/}
                            <TextInput
                                name="podName"
                                style={styles.input}
                                theme={inputTheme}
                                onChangeText={handleChange('podName')}
                                value={values.podName}
                                label={t("_podName")}
                                underlineColor="tomato"
                                selectionColor="tomato"
                                underlineColorAndroid="tomato"
                                testID='podNameInput'
                                keyboardType="default"
                                returnKeyType="done"
                                blurOnSubmit={true}
                                onSubmitEditing={() => {
                                    Keyboard.dismiss()
                                }}
                            />
                            {errors.podName &&
                            <Text style={{fontSize: 10, color: 'red'}}>{errors.podName}</Text>
                            }

                            {/*POD DESCRIPTION*/}
                            <TextInput
                                style={styles.input}
                                theme={inputTheme}
                                onChangeText={handleChange('podDescription')}
                                value={values.podDescription}
                                label={t("_podDescription")}
                                underlineColor="tomato"
                                selectionColor="tomato"
                                multiline={true}
                                keyboardType="default"
                                returnKeyType="done"
                                blurOnSubmit={true}
                                onSubmitEditing={() => {
                                    Keyboard.dismiss()
                                }}
                            />

                            {/*RISK LEVEL MANAGEMENT*/}
                            <Text style={styles.formTitle}>
                                {
                                    renderLevels(riskLevel, t("_riskLevel"))
                                }
                            </Text>
                            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                                <View style={{height: 50}}>
                                    {
                                        riskLevel != 1
                                            ? <TouchableOpacity
                                                onPress={() => {
                                                    setRiskLevel(1);
                                                }}
                                            >
                                                <Button mode="contained" color="grey">
                                                    1
                                                </Button>
                                            </TouchableOpacity>
                                            : <Button mode="contained" color={levelColor.one}>
                                                1
                                            </Button>
                                    }
                                </View>
                                <View style={{height: 50}}>
                                    {
                                        riskLevel != 2
                                            ? <TouchableOpacity
                                                onPress={() => {
                                                    setRiskLevel(2);
                                                }}
                                            >
                                                <Button mode="contained" color="grey">
                                                    2
                                                </Button>
                                            </TouchableOpacity>
                                            : <Button mode="contained" color={levelColor.two}>
                                                2
                                            </Button>
                                    }
                                </View>
                                <View style={{height: 50}}>
                                    {
                                        riskLevel != 3
                                            ? <TouchableOpacity
                                                onPress={() => {
                                                    setRiskLevel(3);
                                                }}
                                            >
                                                <Button mode="contained" color="grey">
                                                    3
                                                </Button>
                                            </TouchableOpacity>
                                            : <Button mode="contained" color={levelColor.three}>
                                                3
                                            </Button>
                                    }
                                </View>
                                <View style={{height: 50}}>
                                    {
                                        riskLevel != 4
                                            ? <TouchableOpacity
                                                onPress={() => {
                                                    setRiskLevel(4);
                                                }}
                                            >
                                                <Button mode="contained" color="grey">
                                                    4
                                                </Button>
                                            </TouchableOpacity>
                                            : <Button mode="contained" color={levelColor.four}>
                                                4
                                            </Button>
                                    }
                                </View>
                                <View style={{height: 50}}>
                                    {
                                        riskLevel != 5
                                            ? <TouchableOpacity
                                                onPress={() => {
                                                    setRiskLevel(5);
                                                }}
                                            >
                                                <Button mode="contained" color="grey">
                                                    5
                                                </Button>
                                            </TouchableOpacity>
                                            : <Button mode="contained" color={levelColor.five}>
                                                5
                                            </Button>
                                    }
                                </View>
                            </View>

                            {/*TECHNICAL LEVEL MANAGEMENT*/}
                            <Text style={styles.formTitle}>
                                {
                                    renderLevels(technicalLevel, t("_technicalLevel"))
                                }
                            </Text>
                            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                                <View style={{height: 50}}>
                                    {
                                        technicalLevel != 1
                                            ? <TouchableOpacity
                                                onPress={() => {
                                                    setTechnicalLevel(1);
                                                }}
                                            >
                                                <Button mode="contained" color="grey">
                                                    1
                                                </Button>
                                            </TouchableOpacity>
                                            : <Button mode="contained" color={levelColor.one}>
                                                1
                                            </Button>
                                    }
                                </View>
                                <View style={{height: 50}}>
                                    {
                                        technicalLevel != 2
                                            ? <TouchableOpacity
                                                onPress={() => {
                                                    setTechnicalLevel(2);
                                                }}
                                            >
                                                <Button mode="contained" color="grey">
                                                    2
                                                </Button>
                                            </TouchableOpacity>
                                            : <Button mode="contained" color={levelColor.two}>
                                                2
                                            </Button>
                                    }
                                </View>
                                <View style={{height: 50}}>
                                    {
                                        technicalLevel != 3
                                            ? <TouchableOpacity
                                                onPress={() => {
                                                    setTechnicalLevel(3);
                                                }}
                                            >
                                                <Button mode="contained" color="grey">
                                                    3
                                                </Button>
                                            </TouchableOpacity>
                                            : <Button mode="contained" color={levelColor.three}>
                                                3
                                            </Button>
                                    }
                                </View>
                                <View style={{height: 50}}>
                                    {
                                        technicalLevel != 4
                                            ? <TouchableOpacity
                                                onPress={() => {
                                                    setTechnicalLevel(4);
                                                }}
                                            >
                                                <Button mode="contained" color="grey">
                                                    4
                                                </Button>
                                            </TouchableOpacity>
                                            : <Button mode="contained" color={levelColor.four}>
                                                4
                                            </Button>
                                    }
                                </View>
                                <View style={{height: 50}}>
                                    {
                                        technicalLevel != 5
                                            ? <TouchableOpacity
                                                onPress={() => {
                                                    setTechnicalLevel(5);
                                                }}
                                            >
                                                <Button mode="contained" color="grey">
                                                    5
                                                </Button>
                                            </TouchableOpacity>
                                            : <Button mode="contained" color={levelColor.five}>
                                                5
                                            </Button>
                                    }
                                </View>
                            </View>

                            {/*COORDINATES MANAGEMENT*/}
                            <Text style={styles.formTitle}>{t("_coordinates")}</Text>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'stretch',}}>
                                <View style={styles.coordinates}>
                                    <Text style={{
                                        textAlignVertical: "center",
                                        textAlign: "center",
                                        fontSize: 18
                                    }}>{latitude.toFixed(4)}° N</Text>
                                </View>
                                <View style={{
                                    width: '20%', height: 45, justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                    {
                                        coordinateGetted
                                            ? <TouchableOpacity
                                                onPress={() => {
                                                    setCoordinateGetted(false);
                                                    findCoordinates();
                                                }}
                                            >
                                                <Icon name="refresh-outline" style={{color: 'tomato'}} size={40}/>
                                            </TouchableOpacity>
                                            : <ActivityIndicator size="large" color="tomato"></ActivityIndicator>
                                    }
                                </View>
                                <View style={styles.coordinates}>
                                    <Text style={{
                                        textAlignVertical: "center",
                                        textAlign: "center",
                                        fontSize: 18
                                    }}>{longitude.toFixed(4)}° E</Text>
                                </View>
                            </View>

                            {/*SEPARATOR*/}
                            <View
                                style={{
                                    borderBottomColor: 'tomato',
                                    borderBottomWidth: 1,
                                    // marginBottom: 16,
                                }}
                            />

                            <View style={styles.container}>
                                <MapView
                                    style={{width: '100%', height: 400}}
                                    mapType="none"
                                    maxZoomLevel={17}
                                    minZoomLevel={7}
                                    region={position}
                                    loadingEnabled={true}
                                    showsUserLocation={true}
                                    provider={PROVIDER_GOOGLE}
                                    scrollEnabled={coordinateGetted}
                                    rotateEnabled={coordinateGetted}
                                    zoomEnabled={coordinateGetted}
                                    pitchEnabled={coordinateGetted}
                                    // showsMyLocationButton={true}
                                    onRegionChangeComplete={(e) => {
                                        if (coordinateGetted) {
                                            // setLatitude(e.latitude);
                                            // setLongitude(e.longitude);
                                            setPosition({
                                                    latitude: e.latitude,
                                                    longitude: e.longitude,
                                                    latitudeDelta: e.latitudeDelta,
                                                    longitudeDelta: e.longitudeDelta
                                                }
                                            );
                                        }
                                    }}
                                >
                                    <UrlTile
                                        urlTemplate={'https://wmts.geo.admin.ch/1.0.0/ch.swisstopo.pixelkarte-grau/default/current/3857/{z}/{x}/{y}.jpeg'}
                                        zIndex={1}
                                    />
                                    <UrlTile
                                        urlTemplate='https://wmts.geo.admin.ch/1.0.0/ch.swisstopo.swisstlm3d-wanderwege/default/current/3857/{z}/{x}/{y}.png'
                                        zIndex={2}
                                    />

                                    <Marker coordinate={{
                                        latitude: latitude,
                                        longitude: longitude
                                    }}
                                            zIndex={10}
                                            draggable={true}
                                            pinColor="red"
                                            onDragEnd={(e) => {
                                                setLatitude(e.nativeEvent.coordinate.latitude);
                                                setLongitude(e.nativeEvent.coordinate.longitude);
                                            }}
                                    />
                                </MapView>

                            </View>
                            {position.latitudeDelta < latitudeDeltaZoomMin && position.longitudeDelta < longitudeDeltaZoomMin
                                ? <Text></Text>
                                : <Text style={{fontSize: 10, color: 'red'}}>{t("_zoomRequired")}</Text>
                            }

                            {/*DATE MANAGEMENT*/}
                            <Text style={styles.formTitle}>{t("_date")}</Text>
                            <DatePicker
                                style={{width: 200}}
                                mode="date"
                                date={podDate}
                                placeholder="select date"
                                format="DD-MM-YYYY"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                disabled
                                customStyles={{
                                    dateIcon: {
                                        position: 'absolute',
                                        left: 0,
                                        top: 4,
                                        marginLeft: 0
                                    },
                                    dateInput: {
                                        marginLeft: 36
                                    }
                                }}
                                value={values.podDate}
                                onValueChange={handleChange('podDate')}
                                onDateChange={(date) => {
                                    setPodDate(date);
                                }}
                            />

                            {/*SUBMIT BUTTON MANAGEMENT*/}
                            {/*Submit button active when all of these are set :
                                - coordinates
                                - levels
                                - image
                                - position accuracy (zoom)
                             */}
                            {
                                latitude != "0"
                                && longitude != "0"
                                && riskLevel != 0
                                && technicalLevel != 0
                                && image
                                && coordinateGetted
                                //Don't forget to change in MyPodDetails
                                && position.latitudeDelta < latitudeDeltaZoomMin
                                && position.longitudeDelta < longitudeDeltaZoomMin
                                    ? <TouchableOpacity onPress={handleSubmit}>
                                        <Button mode="contained" color="tomato" testID="submitButton"
                                                style={styles.button}>{t("_submit")}</Button>
                                    </TouchableOpacity>

                                    : <Button mode="contained" color="tomato" disabled={true}
                                              style={styles.button}>{t("_submit")}</Button>
                            }
                        </ScrollView>
                    )}
                </Formik>
            </View>
        </View>
    );

    //Render the levels
    function renderLevels(level, type) {
        switch (level) {
            case 1:
                return <Text>{type} : {t("_levelOne")}</Text>;
            case 2:
                return <Text>{type} : {t("_levelTwo")}</Text>;
            case 3:
                return <Text>{type} : {t("_levelThree")}</Text>;
            case 4:
                return <Text>{type} : {t("_levelFour")}</Text>;
            case 5:
                return <Text>{type} : {t("_levelFive")}</Text>;
            default:
                return <Text>{type} : </Text>;
        }
    }
}

const inputTheme = {
    colors: {
        primary: 'tomato',
        underlineColor: 'transparent',

    }
};

const styles = StyleSheet.create({
    input: {
        backgroundColor: 'white',
        marginTop: 15,
    },
    container: {
        flex: 1,
        display: "flex",
        justifyContent: 'center',
        alignItems: 'center',
    },
    content: {
        padding: 16,
    },
    button: {
        marginTop: 16,
    },
    coordinates: {width: '40%', height: 20, marginBottom: 10, marginTop: 10},
    formTitle: {
        marginTop: 15,
        marginBottom: 5,
        fontSize: 18,
    },
    touchableZoneImage: {
        width: '100%',
        height: 200,
        backgroundColor: 'white',
        justifyContent: 'center'
    },
    imageSize: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain',
    },
    imageContainer: {
        width: '100%',
        height: 200,
        alignItems: 'center',
        justifyContent: 'center',
        alignContent: 'center',
    }
});
