import Markdown from 'react-native-markdown-display';
import React from "react";
import {ScrollView} from "react-native";
import {locale} from "expo-localization";
import {de, en, fr, it} from "../languages/legal/termsAndConditions.json";
export function TermsAndConditions() {
    //Select terms and conditions according to the language of the phone
    let lang = locale.substring(0, 2);
    if(lang !=='fr' && lang !=='de' && lang !=='it' && lang !=='en'){
        lang='en';
    }
    const termsAndConditions = {
        fr: fr,
        en: en,
        de: de,
        it: it
    }

    return (
        <>
            <ScrollView style={{padding:10, marginBottom:50}}>
                <Markdown>
                    {termsAndConditions[lang]}
                </Markdown>
            </ScrollView>
        </>
    );
}
/*
const termsAndConditions = {
    fr: require('../assets/legal/tc_fr.md'),
    en: require('../assets/legal/tc_en.md'),
    de: require('../assets/legal/tc_de.md'),
    it: require('../assets/legal/tc_it.md')
}*/
