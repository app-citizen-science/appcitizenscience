import React, {useContext, useEffect, useState} from 'react';
import {LoginNavigator} from "./navigation/StackNavigator";
import {NavigationContainer} from "@react-navigation/native";
import * as firebase from 'firebase';
import DrawerNavigator from "./navigation/DrawerNavigator";
import {UserContext} from "./utils/UserContext";
import {getUser} from "./database/firestore_requests";
import {NetworkProvider} from "react-native-offline";
import {Text, View} from "react-native";
import { Feather } from '@expo/vector-icons';
import {t} from "./languages";
import {Button} from 'react-native-paper';


export default function App() {

    const [userAuthenticated, setUserAuthenticated] = useState();
    const [user, setUser] = useState();
    const [podsNumber, setPodsNumber] = useState();
    const [sodsNumber, setSodsNumber] = useState();
    const [offlineError, setOfflineError] = useState();

    useEffect(() => {
            const subscriber = firebase.auth().onAuthStateChanged(userAuth => {
                if (userAuth != null) {
                    setUserAuthenticated(userAuth);
                } else {
                    setUserAuthenticated(undefined);
                }
            });
            return () => subscriber();
        }, [setUserAuthenticated]
    );

    useEffect(() => {
        //Method to get the infos of a user
        if (userAuthenticated !== undefined) {
            const unsubscribe = getUser({
                next: querySnapshot => {
                    const userFirestore = querySnapshot.data();

                    setUser(userFirestore);
                },
                error: (error) => {
                    setUser();
                }
            }, userAuthenticated.uid);
            return () => unsubscribe();
        }
    }, [userAuthenticated]);

    return (

        <>
            <UserContext.Provider value={{
                userAuthenticated: userAuthenticated,
                user: user,
                podsNumber: podsNumber,
                sodsNumber: sodsNumber,
                offlineError: offlineError,
                setUserAuthenticated: setUserAuthenticated,
                setUser: setUser,
                setPodsNumber: setPodsNumber,
                setSodsNumber: setSodsNumber,
                setOfflineError: setOfflineError
            }}>
                <NetworkProvider>
                    <NavigationContainer>
                        {offlineError ?
                            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', minHeight: '10%'}}>
                                <Feather name="info" size={90} color="tomato"/>
                                <Text style={{color: 'tomato', marginTop: 10, fontWeight: 'bold'}}>{t("_offlineError")}</Text>
                                <Button mode="contained" color="tomato" onPress={()=>{setOfflineError(false)}} testID="submitButton" style={{marginTop: 15}}>{t("_tryAgain")}</Button>
                            </View>
                            : (userAuthenticated !== undefined ? <DrawerNavigator/> : <LoginNavigator/>)}
                    </NavigationContainer>
                </NetworkProvider>
            </UserContext.Provider>
        </>
    );

}





