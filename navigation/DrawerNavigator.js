import React, {useContext} from "react";
import {createDrawerNavigator, DrawerContentScrollView, DrawerItem, DrawerItemList} from "@react-navigation/drawer";
import {AboutStackNavigator, DocumentationStackNavigator, SettingsStackNavigator} from "./StackNavigator";
import BottomTabNavigator from "./TabNavigator";
import firebase from "../database/firebase";
import {t} from "../languages";
import {AntDesign} from '@expo/vector-icons';
import {Feather} from '@expo/vector-icons';
import {SimpleLineIcons} from '@expo/vector-icons';
import {MaterialIcons} from '@expo/vector-icons';
import {Alert, Text, View} from "react-native";
import {UserContext} from "../utils/UserContext";

//Drawer management - access to different screens and logout button inside the draw bar
const Drawer = createDrawerNavigator();

function signOut() {

    Alert.alert(
        t("_logout"),
        t("_logoutMessage"),
        [
            {
                text: t("_no"),
                onPress: () => console.log('Canceled deletion of sod'),
                style: 'cancel',
            },
            {
                text: t("_yes"), onPress: async () => {
                    return (
                        firebase.auth().signOut()
                            .catch(error => this.setState({errorMessage: error.message}))

                    );
                }
            },
        ],
        {cancelable: false},
    );

}

const DrawerNavigator = () => {

    let UserType = useContext(UserContext);

    return (
        <Drawer.Navigator initialRouteName="Home" drawerContent={props => {
            return (
                <DrawerContentScrollView {...props}>
                    {
                        UserType.user === undefined || UserType.user === null
                            ? null
                            : <View>
                                <Text style={{
                                    fontWeight: "bold",
                                    fontSize: 16,
                                    marginTop: 10,
                                    textAlign: "center"
                                }}>{UserType.user.role}</Text>
                                <Text style={{color: "gray", textAlign: "center"}}>{UserType.user.username}</Text>
                                <Text style={{color: "gray", textAlign: "center"}}>{UserType.user.email}</Text>
                            </View>
                    }

                    <View
                        style={{
                            borderBottomColor: 'tomato',
                            borderBottomWidth: 1,
                            marginTop: "8%",
                            marginBottom: "8%",
                        }}
                    />
                    <DrawerItemList {...props} />
                    <DrawerItem label={t("_logout")}
                                icon={({size, color}) => (
                                    <MaterialIcons name="logout" size={size} color={color}/>
                                )}
                                onPress={() => signOut()}/>
                </DrawerContentScrollView>
            )
        }
        }
                          drawerContentOptions={{
                              activeTintColor: 'tomato',
                              itemStyle: {marginVertical: 3},
                          }}>


            <Drawer.Screen name="Home" component={BottomTabNavigator}

                           options={{
                               title: t("_home"),
                               drawerIcon: ({size, color}) => (
                                   <AntDesign name="home" size={size} color={color}/>
                               ),
                           }}

            />
            <Drawer.Screen name="About" component={AboutStackNavigator}

                           options={{
                               title: t("_about"),
                               drawerIcon: ({size, color}) => (
                                   <AntDesign name="infocirlceo" size={size} color={color}/>
                               ),
                           }}
            />

            <Drawer.Screen name="Documentation" component={DocumentationStackNavigator}

                           options={{
                               title: t("_documentation"),
                               drawerIcon: ({size, color}) => (
                                   <SimpleLineIcons name="docs" size={size} color={color}/>
                               ),
                           }}

            />
        </Drawer.Navigator>
    );
}

export default DrawerNavigator;
