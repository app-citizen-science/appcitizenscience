import React, { useEffect } from 'react'
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native'
import firebase from '../database/firebase';
import {t} from "../languages";




//The user will be brought from the login to the menu (Bottom tabs navigator)

const SwitchNavigator = ({navigation}) => {
    useEffect(() => {
        firebase.auth().onAuthStateChanged(user => {
            navigation.navigate(user && user.emailVerified ? "BottomTabsNavigator" : "AuthStackNavigator")
        })
      },
        [])


    return (

        <View style={styles.container}>
            <Text style={styles.text}>{t("_loading")}</Text>
            <ActivityIndicator size="large" color="tomato"/>
        </View>


    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text:{
        color: 'tomato',
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 30
    }

})

export default SwitchNavigator
