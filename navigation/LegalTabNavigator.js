import React from "react";
import {createMaterialTopTabNavigator} from "@react-navigation/material-top-tabs";
import {t} from "../languages";
import {Ionicons, MaterialCommunityIcons} from '@expo/vector-icons';
import {PrivacyPolicy} from "../screens/PrivacyPolicy";
import {TermsAndConditions} from "../screens/TermsAndConditions";

//Manage the two tabbed menu for the legal documentation

const Tab = createMaterialTopTabNavigator();

export function LegalTabNavigator({navigation}){
    return (
        <Tab.Navigator
            initialRouteName='PrivacyPolicy'
            screenOptions={({route}) => ({
                tabBarIcon: ({focused, color, size}) => {
                    if (route.name === 'PrivacyPolicy') {
                        return (
                            <MaterialCommunityIcons name="shield-alert" size={24} color={color}/>
                        );
                    } else if (route.name === 'TermsAndConditions') {
                        return (
                            <Ionicons name="warning" size={24} color={color}/>
                        );
                    }
                },
            })}
            tabBarOptions={{
                labelStyle: {textTransform: 'none'},
                showIcon: true,
                activeTintColor: 'tomato',
                inactiveTintColor: 'black',
                indicatorStyle: {backgroundColor: 'tomato'}
            }}
        >
            <Tab.Screen name="PrivacyPolicy" component={PrivacyPolicy}
                        options={{
                            title: t("_privacyPolicy")
                        }}
            />
            <Tab.Screen name="TermsAndConditions" component={TermsAndConditions}
                        options={{
                            title: t("_termsAndConditions")
                        }}
            />
        </Tab.Navigator>
    );
}
