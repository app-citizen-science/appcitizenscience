import React from "react";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {Ionicons} from '@expo/vector-icons';
import {MapNavigator, PodsNavigator, SodsNavigator} from "./StackNavigator";
import {t} from "../languages";
import {FontAwesome5, Entypo} from '@expo/vector-icons';

//Manage the two tabbed menu : map and my pods, the home screen of the app

const Tab = createBottomTabNavigator();

const BottomTabNavigator = (navigation) => {
    return (
        <Tab.Navigator
            initialRouteName='Map'
            screenOptions={({route}) => ({
                tabBarIcon: ({focused, color, size}) => {
                    if (route.name === 'My Pod') {
                        return (
                            <FontAwesome5 name="map-marker-alt" size={24} color={color}/>
                        );
                    } else if (route.name === 'My Sod') {
                        return (
                            <FontAwesome5 name="route" size={24} color={color}/>
                        );
                    } else if (route.name === 'Map') {
                        return (
                            <Entypo name="map" size={24} color={color}/>
                        );
                    }
                },
            })}
            tabBarOptions={{
                activeTintColor: 'tomato',
                inactiveTintColor: 'black',
            }}
        >
            <Tab.Screen name="My Pod" component={PodsNavigator}
                        options={{
                            title: t("_myPod")
                        }}
            />
            <Tab.Screen name="My Sod" component={SodsNavigator}
                        options={{
                            title: t("_mySod")
                        }}
            />
            <Tab.Screen name="Map" component={MapNavigator}
                        options={{
                            title: t("_map")
                        }}
            />
        </Tab.Navigator>
    );
};

export default BottomTabNavigator;
