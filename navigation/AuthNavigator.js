import React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import Login from '../screens/Login';
import Signup from '../screens/Signup';
import {t} from "../languages";
import {LegalTabNavigator} from "./LegalTabNavigator";

const Stack = createStackNavigator();

//Login and sign up navigation screen

export default function AuthStack() {
    return (
        <Stack.Navigator
            initialRouteName='Login'
            screenOptions={{
                headerTitleAlign: 'center',
                headerStyle: {
                    backgroundColor: 'tomato',
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: 'bold',
                },
            }}
        >
            <Stack.Screen
                name="Signup"
                component={Signup}
                options={{
                    title: 'SanTour',
                    headerTitleStyle: {fontSize: 24},
                    headerLeft: () => null,
                    gestureEnabled: false
                }}

            />
            <Stack.Screen
                name="SignupLegal"
                component={LegalTabNavigator}
                options={{
                    title: t("_legalDocumentation"),
                    headerBackTitle: t('_back'),
                    headerBackAllowFontScaling: true,
                }}
            />
            <Stack.Screen
                name="Login"
                component={Login}
                options={
                    {
                        title: null,
                        headerLeft: () => null,
                        gestureEnabled: false,
                    }
                }
            />

        </Stack.Navigator>
    );
}
