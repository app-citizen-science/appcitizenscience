import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import SwitchNavigator from './SwitchNavigator'
import BottomTabNavigator from "./TabNavigator";
import AuthStack from "./AuthNavigator";
import About from "../screens/About"
import Documentation from "../screens/Documentation";
import Settings from "../screens/Settings"
import Header from "../utils/Header";
import {t} from "../languages";
import {NewPodScreen} from "../screens/NewPod";
import {MyPodDetails} from "../screens/MyPodDetails";
import {MyPods} from "../screens/MyPods";
import {MapScreen} from "../screens/Map";
import {NewSodScreen} from "../screens/NewSod";
import {MySods} from "../screens/MySods";
import {MySodDetails} from "../screens/MySodDetails";
import Signup from "../screens/Signup";
import {LegalTabNavigator} from "./LegalTabNavigator";
import Technicality from "../screens/Technicality";
import Risk from "../screens/Risk";

//Stack navigation management, navigation through different screens

const Stack = createStackNavigator();

const LoginNavigator = () => {
    return (
        <Stack.Navigator initialRouteName='Loading' headerMode='none'>
            <Stack.Screen name='Loading' component={SwitchNavigator}/>
            <Stack.Screen name='AuthStackNavigator' component={AuthStack} options={{
                gestureEnabled: false,
            }}/>
            <Stack.Screen name='BottomTabsNavigator' component={BottomTabNavigator}/>
        </Stack.Navigator>
    );
}

const PodsNavigator = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="My POD" component={MyPods}
                          options={
                              ({navigation}) => {
                                  return {
                                      headerTitle: () => <Header navigation={navigation} title={t("_myPod")}/>
                                  }
                              }
                          }
            />
            <Stack.Screen name="New POD" component={NewPodScreen}
                          options={{
                              title: t("_newPod"),
                              headerTintColor: 'tomato',
                              headerBackTitle: t('_back'),
                          }}
            />
            <Stack.Screen name="MyPodDetails" component={MyPodDetails}
                          options={{
                              title: t("_podsDetailsEditOff"),
                              headerTintColor: 'tomato',
                              headerBackTitle: t('_back'),
                          }}
            />
        </Stack.Navigator>
    );
}


export const SodsNavigator = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="My SOD" component={MySods}
                          options={
                              ({navigation}) => {
                                  return {
                                      headerTitle: () => <Header navigation={navigation} title={t("_mySod")}/>
                                  }
                              }
                          }
            />
            <Stack.Screen name="New SOD" component={NewSodScreen}
                          options={{
                              title: t("_newSod"),
                              headerTintColor: 'tomato',
                              headerBackTitle: t('_back'),
                          }}
            />
            <Stack.Screen name="MySodDetails" component={MySodDetails}
                          options={{
                              title: t("_sodsDetailsEditOff"),
                              headerTintColor: 'tomato',
                              headerBackTitle: t('_back'),
                          }}
            />
        </Stack.Navigator>
    );
}


const MapNavigator = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Map" component={MapScreen}
                          options={
                              ({navigation}) => {
                                  return {
                                      headerTitle: () => <Header navigation={navigation} title={t("_map")}/>
                                  }
                              }
                          }
            />
            <Stack.Screen name="MyPodDetails" component={MyPodDetails}
                          options={{
                              title: t("_podsDetailsEditOn"),
                              headerTintColor: 'tomato',
                          }}
            />
            <Stack.Screen name="MySodDetails" component={MySodDetails}
                          options={{
                              title: t("_sodsDetailsEditOn"),
                              headerTintColor: 'tomato',
                          }}
            />
        </Stack.Navigator>
    );
}


const AboutStackNavigator = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="About" component={About}
                          options={
                              ({navigation}) => {
                                  return {
                                      headerTitle: () => <Header navigation={navigation} title={t("_about")}/>
                                  }
                              }
                          }
            />
            <Stack.Screen
                name="Legal"
                component={LegalTabNavigator}
                options={{
                    title: t("_legalDocumentation"),
                    headerBackTitle: t('_back'),
                    headerTintColor: 'tomato',
                    headerBackAllowFontScaling: true,
                }}
            />
        </Stack.Navigator>
    );
}


const DocumentationStackNavigator = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Documentation" component={Documentation}
                          options={
                              ({navigation}) => {
                                  return {
                                      headerTitle: () => <Header navigation={navigation} title={t("_documentation")}/>
                                  }
                              }
                          }/>
            <Stack.Screen
                name="Risk"
                component={Risk}
                options={{
                    title: t("_riskDocTitle"),
                    headerBackTitle: t('_back'),
                    headerTintColor: 'tomato',
                    headerBackAllowFontScaling: true,
                }}
            />
            <Stack.Screen
                name="Technicality"
                component={Technicality}
                options={{
                    title: t("_technicalDocTitle"),
                    headerBackTitle: t('_back'),
                    headerTintColor: 'tomato',
                    headerBackAllowFontScaling: true,
                }}
            />
        </Stack.Navigator>
    );
}


const SettingsStackNavigator = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Settings" component={Settings}
                          options={
                              ({navigation}) => {
                                  return {
                                      headerTitle: () => <Header navigation={navigation} title={t("_settings")}/>
                                  }
                              }
                          }/>

        </Stack.Navigator>
    );
}








export {LoginNavigator, AboutStackNavigator, SettingsStackNavigator, DocumentationStackNavigator, PodsNavigator, MapNavigator};
