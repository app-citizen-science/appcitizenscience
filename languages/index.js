import I18n from 'ex-react-native-i18n'
import en from './en.json'
import fr from './fr.json'
import it from './it.json'
import de from './de.json'
import {locale, Localization} from "expo-localization";
import i18n from "ex-react-native-i18n";



I18n.translations = {
    en,
    de,
    fr,
    it
}


const getLanguage = async() => {
    i18n.locale = locale.substr(0,2);
    i18n.fallbacks = true;
}


    getLanguage()

    export function t(name){
        return I18n.t(name)
    }



