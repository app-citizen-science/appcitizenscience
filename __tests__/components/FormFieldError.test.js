import React from 'react';
import renderer from 'react-test-renderer';
import {FormFieldError} from "../../components/FormFieldError";

test('Renders correctly', () => {
    const tree = renderer.create(<FormFieldError text="Test Example"/>).toJSON();
    expect(tree).toMatchSnapshot();
});


