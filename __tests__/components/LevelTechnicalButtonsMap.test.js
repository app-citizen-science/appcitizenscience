import React from 'react';
import renderer from 'react-test-renderer';
import {LevelTechnicalButtonsMap} from "../../components/LevelTechnicalButtonsMap";

test('Renders correctly', () => {
    const tree = renderer.create(<LevelTechnicalButtonsMap elementSelected={5}/>).toJSON();
    expect(tree).toMatchSnapshot();
});


