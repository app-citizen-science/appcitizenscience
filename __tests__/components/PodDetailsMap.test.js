import React from 'react';
import renderer from 'react-test-renderer';
import {PodDetailsMap} from "../../components/PodDetailsMap";
import firebase from "firebase";
import {NetworkConsumer, NetworkProvider, useIsConnected} from "react-native-offline";
import {UserContext} from "../../utils/UserContext";

test('Renders correctly', () => {
    const podTest = {
        pod_id: 1,
        doc_id: "TESTUID",
        fk_user_id: "TESTUSERID",
        date: new firebase.firestore.FieldValue.serverTimestamp(),
        description: "TESTDESCRIPTION",
        geo_position: new firebase.firestore.GeoPoint(46.0004, 7.35),
        img_download_url: "",
        level_risk: 2,
        level_technical: 1,
        name: "TESTNAME",
        status: "validated",
        modification_date: new firebase.firestore.FieldValue.serverTimestamp(),
        language: "fr",
    };
    const navigation = {
        "navigate": () => console.log('Go back'),
    }


    const tree = renderer.create(
        <UserContext.Provider value={{
            user: {role: "Expert"}
        }}>
            <NetworkProvider>
                <PodDetailsMap
                    pod={podTest}
                    closeDetailsPanel={() => console.log('close test')}
                    ownerElement={"OWNERNAMETEST"}
                    navigation={navigation}
                />
            </NetworkProvider>
        </UserContext.Provider>
    ).toJSON();
    expect(tree).toMatchSnapshot();
});
