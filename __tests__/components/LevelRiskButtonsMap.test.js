import React from 'react';
import renderer from 'react-test-renderer';
import {LevelRiskButtonsMap} from "../../components/LevelRiskButtonsMap";

test('Renders correctly', () => {
    const tree = renderer.create(<LevelRiskButtonsMap elementSelected={5}/>).toJSON();
    expect(tree).toMatchSnapshot();
});


