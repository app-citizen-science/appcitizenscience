import renderer from "react-test-renderer";
import React from "react";
import Signup from "../../screens/Signup";

test('Renders correctly', () => {
    const tree = renderer.create(<Signup/>).toJSON();
    expect(tree).toMatchSnapshot();
});
