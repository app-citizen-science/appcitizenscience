import React from 'react';
import renderer from 'react-test-renderer';
import Login from "../../screens/Login";

test('Renders correctly', () => {
    const tree = renderer.create(<Login/>).toJSON();
    expect(tree).toMatchSnapshot();
});
