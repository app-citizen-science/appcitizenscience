import React from 'react';
import renderer from 'react-test-renderer';
import {PrivacyPolicy} from "../../screens/PrivacyPolicy";

test('Renders correctly', () => {
    const tree = renderer.create(<PrivacyPolicy/>).toJSON();
    expect(tree).toMatchSnapshot();
});
