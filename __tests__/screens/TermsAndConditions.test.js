import React from 'react';
import renderer from 'react-test-renderer';
import {TermsAndConditions} from "../../screens/TermsAndConditions";

test('Renders correctly', () => {
    const tree = renderer.create(<TermsAndConditions/>).toJSON();
    expect(tree).toMatchSnapshot();
});
