import React from 'react';
import renderer from 'react-test-renderer';
import About from "../../screens/About";

test('Renders correctly', () => {
    const tree = renderer.create(<About/>).toJSON();
    expect(tree).toMatchSnapshot();
});
