import * as React from "react";
import {useIsConnected} from "react-native-offline";
import {View} from "react-native";
import {Banner} from "react-native-paper";
import {Ionicons} from "@expo/vector-icons";
import {t} from "../languages";

const ConnectionManagement = () => {
    const [visible, setVisible] = React.useState(true);
    const isConnected = useIsConnected();

    return (
        <View>
            {
                isConnected
                    ? <View></View>
                    : <Banner
                        visible={visible}
                        actions={[
                            {
                                // label: 'Understood',
                                // color:'tomato',
                                // onPress: () => setVisible(false),
                            },
                        ]}
                        style={{height:75, marginTop:-10}}
                        icon={({size}) => (
                            <Ionicons name="alert-circle-outline" size={size} color="tomato"/>
                        )}
                    >
                        {t('_offlineMode')}
                    </Banner>
            }
        </View>
    );
};

export default ConnectionManagement;
