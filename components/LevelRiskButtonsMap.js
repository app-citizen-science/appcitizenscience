import {ScrollView, StyleSheet, Text, View} from "react-native";
import {t} from "../languages";
import {Button} from "react-native-paper";
import * as React from "react";

export function LevelRiskButtonsMap(props) {
    const elementSelected = props.elementSelected;

    return (
        <>
            <Text style={{color: 'tomato'}}>{t('_riskLevel').concat(' : ')}</Text>
            {elementSelected.level_risk === 1 ? (
                <Button mode="contained" color={levelColor.one}>
                    {elementSelected.level_risk}
                </Button>
            ) : elementSelected.level_risk === 2 ? (
                <Button mode="contained" color={levelColor.two}>
                    {elementSelected.level_risk}
                </Button>
            ) : elementSelected.level_risk === 3 ? (
                <Button mode="contained" color={levelColor.three}>
                    {elementSelected.level_risk}
                </Button>
            ) : elementSelected.level_risk === 4 ? (
                <Button mode="contained" color={levelColor.four}>
                    {elementSelected.level_risk}
                </Button>
            ) : (
                <Button mode="contained" color={levelColor.five}>
                    {elementSelected.level_risk}
                </Button>
            )}
        </>
    );
}

let levelColor = {
    one: 'green',
    two: 'blue',
    three: 'orange',
    four: 'red',
    five: 'black'
}
