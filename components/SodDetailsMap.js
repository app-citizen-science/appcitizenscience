import {Image, ScrollView, StyleSheet, Text, View} from "react-native";
import {Entypo, MaterialIcons, Ionicons} from "@expo/vector-icons";
import {TextInput} from "react-native-paper";
import {t} from "../languages";
import {Button as NativeButton} from "react-native-elements";
import * as React from "react";
import {useContext, useState} from "react";
import {UserContext} from "../utils/UserContext";
import {useIsConnected} from "react-native-offline";
import {LevelRiskButtonsMap} from "./LevelRiskButtonsMap";
import {LevelTechnicalButtonsMap} from "./LevelTechnicalButtonsMap";


export function SodDetailsMap(props) {
    //This component displays the details of a SOD on the map

    //Get values from the props
    let sodSelected = props.sod;
    let closeDetailsPanel = props.closeDetailsPanel;
    let ownerElement = props.ownerElement;
    let navigation = props.navigation;

    //States
    const [imageDownloadError, setImageDownloadError] = useState(false);

    //Hooks
    let userContext = useContext(UserContext);
    const isConnected = useIsConnected();

    return (
        <>
            <View style={{flex: 1, paddingTop: 40}}>
                <View style={{position: 'absolute', width: '100%', top: 1}}>
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <Entypo name="chevron-down" size={32} color="tomato"
                                onPress={(event) => closeDetailsPanel(event)}/>
                    </View>
                </View>

                <ScrollView style={{flex: 1, flexDirection: 'column'}}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                        <View style={{
                            flexDirection: 'row',
                            width: '30%',
                            height: '100%',
                            alignContent: 'center',
                            alignItems: 'center'
                        }}>
                            {sodSelected.img_download_url !== '' ? (
                                    imageDownloadError === false ? (
                                        <Image style={{width: 100, height: 150}}
                                               source={{uri: sodSelected.img_download_url, cache: 'force-cache'}}
                                               onError={() => setImageDownloadError(true)}
                                        />
                                    ): <MaterialIcons name="image-not-supported" size={100} color="grey" />
                                ) :
                                <View style={{width: 100, height: 100, alignContent: "center"}}>
                                    <Ionicons name="image-outline" size={100} color="grey"/>
                                </View>
                            }
                        </View>
                        <ScrollView>
                            <TextInput
                                style={styles.input}
                                theme={inputTheme}
                                value={sodSelected.name}
                                label={t("_sodName")}
                                underlineColor="tomato"
                                selectionColor="tomato"
                                editable={false}
                            />
                            <TextInput
                                style={styles.input}
                                theme={inputTheme}
                                value={sodSelected.description}
                                label={t("_sodDescription")}
                                underlineColor="tomato"
                                selectionColor="tomato"
                                editable={false}
                                multiline={true}
                            />
                            <TextInput
                                style={styles.input}
                                theme={inputTheme}
                                value={isConnected ? ownerElement : sodSelected.uid}
                                label={t("_owner")}
                                underlineColor="tomato"
                                selectionColor="tomato"
                                editable={false}
                            />
                            <View style={styles.levelContainer}>
                                <LevelRiskButtonsMap elementSelected={sodSelected}/>
                            </View>
                            <View style={styles.levelContainer}>
                                <LevelTechnicalButtonsMap elementSelected={sodSelected}/>
                            </View>
                        </ScrollView>

                    </View>
                    {userContext.user.role === 'Administrator' || userContext.user.role === 'Expert' ?
                        (isConnected ?
                            <View style={{marginBottom: 10, alignItems: 'center'}}>
                                <NativeButton
                                    title={t('_editSod')}
                                    type="solid"
                                    onPress={() => navigation.navigate('MySodDetails', {
                                        doc_id: sodSelected.doc_id,
                                        riskLvl: sodSelected.level_risk,
                                        technicalLvl: sodSelected.level_technical,
                                        from_map: true
                                    })}
                                    icon={
                                        <MaterialIcons name="edit" size={22} color="white"/>
                                    }/>

                            </View> : null) : null}
                </ScrollView>
            </View>
        </>
    );

}

const styles = StyleSheet.create({
    levelContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 5,
    },
    input: {
        backgroundColor: 'white',
        marginBottom: 5
    },
});


const inputTheme = {
    colors: {
        primary: 'tomato',
        underlineColor: 'transparent',
    }
}


