import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';

export function FormFieldError(props){
    //Get the text in the props
    const textError = props.text;

    return (
        <>
            <View style={styles.errorContainer}>
                <MaterialIcons name="error-outline" size={15} color="red"/>
                <Text style={styles.errorTextStyle}>{textError}</Text>
            </View>
        </>
    )
}

const styles = StyleSheet.create({
    errorContainer :{
        marginTop: 5,

        display: 'flex',
        flexDirection: 'row',

    },
    errorTextStyle: {
        marginLeft: 5,
        marginBottom: 5,
        fontSize: 10,
        color: 'red'
    }
});
