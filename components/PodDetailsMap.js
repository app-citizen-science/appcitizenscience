import {Image, ScrollView, StyleSheet, View} from "react-native";
import {Entypo, Ionicons, MaterialIcons} from "@expo/vector-icons";
import {TextInput} from "react-native-paper";
import {t} from "../languages";
import {LevelRiskButtonsMap} from "./LevelRiskButtonsMap";
import {LevelTechnicalButtonsMap} from "./LevelTechnicalButtonsMap";
import {Button as NativeButton} from "react-native-elements";
import * as React from "react";
import {useContext, useState} from "react";
import {UserContext} from "../utils/UserContext";
import {useIsConnected} from "react-native-offline";

export function PodDetailsMap(props) {
    //This component displays the details of a SOD on the map

    //Get values from the props
    let podSelected = props.pod;
    let closeDetailsPanel = props.closeDetailsPanel;
    let ownerElement = props.ownerElement;
    let navigation = props.navigation;

    //States
    const [imageDownloadError, setImageDownloadError] = useState(false);

    //Hooks
    let userContext = useContext(UserContext);
    const isConnected = useIsConnected();

    return (
            <View style={{flex: 1, paddingTop: 40}}>
                <View style={{position: 'absolute', width: '100%', top: 1}}>
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <Entypo name="chevron-down" size={32} color="tomato"
                                onPress={(event) => closeDetailsPanel(event)}/>
                    </View>
                </View>

                <ScrollView style={{flex: 1, flexDirection: 'column'}}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                        <View style={{
                            flexDirection: 'row',
                            width: '30%',
                            height: '100%',
                            alignContent: 'center',
                            alignItems: 'center'
                        }}>
                            {podSelected.img_download_url !== '' ? (
                                    imageDownloadError === false ? (
                                        <Image style={{width: 100, height: 150}}
                                               source={{uri: podSelected.img_download_url, cache: 'force-cache'}}
                                               onError={() => setImageDownloadError(true)}
                                        />
                                    ): <MaterialIcons name="image-not-supported" size={100} color="grey" />
                                ) :
                                <View style={{width: 100, height: 100, alignContent: "center"}}>
                                    <Ionicons name="image-outline" size={100} color="grey"/>
                                </View>
                            }
                        </View>
                        <ScrollView>
                            <TextInput
                                style={styles.input}
                                theme={inputTheme}
                                value={podSelected.name}
                                label={t("_podName")}
                                underlineColor="tomato"
                                selectionColor="tomato"
                                editable={false}
                            />
                            <TextInput
                                style={styles.input}
                                theme={inputTheme}
                                value={podSelected.description}
                                label={t("_podDescription")}
                                underlineColor="tomato"
                                selectionColor="tomato"
                                editable={false}
                                multiline={true}
                            />
                            <TextInput
                                style={styles.input}
                                theme={inputTheme}
                                value={isConnected ? ownerElement : podSelected.uid}
                                label={t("_owner")}
                                underlineColor="tomato"
                                selectionColor="tomato"
                                editable={false}
                            />
                            <View style={styles.levelContainer}>
                                <LevelRiskButtonsMap elementSelected={podSelected}/>
                            </View>
                            <View style={styles.levelContainer}>
                                <LevelTechnicalButtonsMap elementSelected={podSelected}/>
                            </View>
                        </ScrollView>

                    </View>
                    {userContext.user.role === 'Administrator' || userContext.user.role === 'Expert' ?
                        (isConnected ?
                            <View style={{marginBottom: 10, alignItems: 'center'}}>
                                <NativeButton
                                    title={t('_editPod')}
                                    type="solid"
                                    onPress={() => navigation.navigate('MyPodDetails', {doc_id: podSelected.doc_id, riskLvl: podSelected.level_risk, technicalLvl: podSelected.level_technical, from_map: true})}
                                    icon={
                                        <MaterialIcons name="edit" size={22} color="white"/>
                                    }/>

                            </View> : null) : null}
                </ScrollView>
            </View>
    )
}


const styles = StyleSheet.create({
    levelContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 5,
    },
    input: {
        backgroundColor: 'white',
        marginBottom: 5
    },
});


const inputTheme = {
    colors: {
        primary: 'tomato',
        underlineColor: 'transparent',
    }
}
