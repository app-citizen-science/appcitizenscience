import {Text, View} from "react-native";
import {t} from "../languages";
import {Button} from "react-native-paper";
import * as React from "react";

export function LevelTechnicalButtonsMap(props){
    const elementSelected = props.elementSelected;

    return (
        <>
            <Text style={{color: 'tomato'}}>{t('_technicalLevel').concat(' : ')}</Text>
            {elementSelected.level_technical === 1 ? (
                <Button mode="contained" color={levelColor.one}>
                    {elementSelected.level_technical}
                </Button>
            ) : elementSelected.level_technical === 2 ? (
                <Button mode="contained" color={levelColor.two}>
                    {elementSelected.level_technical}
                </Button>
            ) : elementSelected.level_technical === 3 ? (
                <Button mode="contained" color={levelColor.three}>
                    {elementSelected.level_technical}
                </Button>
            ) : elementSelected.level_technical === 4 ? (
                <Button mode="contained" color={levelColor.four}>
                    {elementSelected.level_technical}
                </Button>
            ) : (
                <Button mode="contained" color={levelColor.five}>
                    {elementSelected.level_technical}
                </Button>
            )}
        </>
    );
}

let levelColor = {
    one: 'green',
    two: 'blue',
    three: 'orange',
    four: 'red',
    five: 'black'
}
